# Website UAS Business Intelligence

### Yang Dibutuhkan

* **PHP >= 7.25**
* **Composer**

### Installing

Clone Repo

```
git clone https://bitbucket.org/Nvnt18/uas-bi.git

```
Pindah ke directory project

```
cd uas-bi
```

Copy file .env.example menjadi .env dengan syntax

```
cp .env.example .env
```

Jalankan Composer Install / Composer Update

```
composer install / update
```

Generate App Key

```
php artisan key:generate
```

Buat Database dengan Nama **classicmodels**

Setting Koneksi Database di file **.env** pada root directory project

Jalankan Migration dan Seeder Awal

```
php artisan migrate:fresh --seed
```

Jalankan Aplikasi

```
php artisan serve
```

## Built With

* [Laravel V.7](https://laravel.com/)


## Authors

* **UAS Business Intelligence**
* **Novianto Indra Kusuma**