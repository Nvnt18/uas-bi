var appScript = {
    base_url: null,
    notifikasi: function(pesan,warna=null,posisi="top-right"){
    	var loaderBg = {}
    	loaderBg.success = '#f96868';
    	loaderBg.info = '#46c35f';
    	loaderBg.error = '#f2a654';
    	loaderBg.warning = '#57c7d4';

    	var title = {}
    	title.success = 'Sukses';
    	title.info = 'Info';
    	title.error = 'Error';
    	title.warning = 'Peringatan';

	    $.toast({
	      heading: warna? title[warna] : title.info,
	      text: pesan,
	      showHideTransition: 'slide',
	      icon: warna? warna : 'info',
	      loaderBg: warna ? loaderBg[warna] : loaderBg.info,
	      position: posisi
	    })
    },
    addWeekdays:function(date, days){
        var moment_date = moment(date);
        while (days > 0) {
            moment_date = moment_date.add(1,'d');
            if (moment_date.isoWeekday() != 6 && moment_date.isoWeekday() != 7) {
                days -= 1;
            }
        }
        return moment_date;
    },
    randomStringNumber: function (length) {
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	    for (var i = 0; i < length; i++)
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
	randomString: function (length) {
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	    for (var i = 0; i < length; i++)
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
	randomStringUpCase: function (length) {
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	    for (var i = 0; i < length; i++)
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
	randomStringLowCase: function (length) {
	    var text = "";
	    var possible = "abcdefghijklmnopqrstuvwxyz";

	    for (var i = 0; i < length; i++)
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
	randomNumber: function (length) {
	    var text = "";
	    var possible = "0123456789";

	    for (var i = 0; i < length; i++)
	        text += possible.charAt(Math.floor(Math.random() * possible.length));

	    return text;
	},
    hanyaAngka: function(){
    	$('.hanya-angka').keypress(function(evt) {
	        var charCode = (evt.which) ? evt.which : event.keyCode
	        if (charCode > 31 && (charCode < 48 || charCode > 57))
	            return false;
	        return true;
    	});
    },
    toDate: function (val) {
	    if (val != null) {
        var from = val.split("-");
	        return (from[2]+'-'+from[1]+'-'+ from[0]);
	    }else {
	        return '';
	    }
    },
    formatMoney: function (a, c, d, t) {
        // a = 47000
        // c = 2
        // d = ,
        // t = .
        // result = 47.000,00
        var n = a,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    reverseFormatMoney : function(teks){
        var hasil = teks.replace(/\D/g, '');
        return hasil;
    },
    datePicker: function(){
    	$('.tgl').attr('data-toggle', 'datetimepicker');
        $('.tgl').datetimepicker({
            format: 'DD-MM-YYYY',
            autoclose: true,
        });
    },
    timePicker: function(){
    	$('.jam').attr('data-toggle', 'datetimepicker');
        $('.jam').datetimepicker({
        	locale:'id',
            format: 'LT',
            autoclose: true,
        });
    },
    monthYearPicker: function(){
        $('.bulan_tahun').attr('data-toggle', 'datetimepicker');
        $('.bulan_tahun').datetimepicker({
            locale:'id',
            format: 'MM-YYYY',
            viewMode: 'years'
        });
    },
    yearPicker: function(){
    	$('.tahun').attr('data-toggle', 'datetimepicker');
        $('.tahun').datetimepicker({
        	locale:'id',
            format: 'YYYY',
            viewMode: 'years',
            autoclose: true,

        });
    },
    daterangePicker: function(){
    	$('.tgl_range').daterangepicker({
            "showDropdowns": true,
            "linkedCalendars": false,
            "locale" :{
                "separator": " s.d. ",
                "applyLabel": "Pilih",
                "cancelLabel": "Batal",
                "fromLabel": "Dari",
                "toLabel": "Sampai",
                "format" : "DD-MM-YYYY"
            },
            "cancelClass": "btn-danger"
        });
    },
    combobox: function(allowClear = false){
    	$(".cmb_select2").css('width', '100%');
    	$(".cmb_select2").select2({
            placeholder:"Pilih salah satu",
            allowClear:allowClear,
    	});
    },
   	editor: function(){
	    $('.editor').summernote({
	      height: 200,
	      // tabsize: 2
	    });
   	},
   	autoNumeric:function(){
	    $('.autonumeric').autoNumeric('init', {
	        aSep: '.', 
	        aDec: ',',
	        aForm: true,
	        vMax: '999999999999999',
	        vMin: '0',
	        mDec: 0,
	    });
   	},
   	autoNumericPercentage:function(){
	    $('.autonumeric').autoNumeric('init', {
	        aSep: '', 
	        aDec: '.',
	        aForm: true,
	        vMax: '100',
	        vMin: '0',
            mDec: 0
	    });
   	},
    switcher:function(){
        var elem = Array.prototype.slice.call(document.querySelectorAll('.switcher'));
        elem.forEach(function(html) {
            var switchery = new Switchery(html, {
                color: '#4099ff',
                jackColor: '#fff'
            });
        });
        // var switchery = new Switchery(elemprimary, {
        //     color: '#4099ff',
        //     jackColor: '#fff'
        // });
    },
    tags:function () {
        $('.tags').tagsinput('items');
    },
   	blockUI: function(){
   		$.blockUI({ message: '<div id="loader"><div id="shadow"></div><div id="box"></div></div>',
              css: {  border: 'none', 
                      padding: '10px', 
                      backgroundColor: 'transparant', 
                      '-webkit-border-radius': '10px', 
                      '-moz-border-radius': '10px', 
                      opacity: 1
            },
            baseZ: 10080
      });
   	},
    blockUICustomImage: function(imageUrl, message = "Loading . . ."){
        $.blockUI({ message: '<text style="font-size:20px"> <img src="'+imageUrl+'" />&nbsp;&nbsp;'+message+'</text>',
              css: {  border: 'none', 
                      paddingTop: '25px', 
                      paddingBottom: '25px', 
                      backgroundColor: 'white', 
                      '-webkit-border-radius': '10px', 
                      '-moz-border-radius': '10px', 
                      opacity: 1
            },
            baseZ: 10080
        });
    },
    unblockUI: function(){
        $.unblockUI();
    },
    swalert: function(title, content, type = "info", callback = null , param = null) {
    	swal(title, content, type).then((value) => {
            if(callback){
                callback(param);
            }
        });
  	},
  	swconfirm: function(title,content,callback,param=null) {
	    swal({
	        title: title,
	        text: content,
	        type: "warning",
		  	buttons: {
			  confirm: {
			    text: "Ya",
			    value: true,
			    visible: true,
			    className: "",
			    closeModal: true
			  },
			   cancel: {
			    text: "Tidak",
			    value: false,
			    visible: true,
			    className: "",
			    closeModal: true,
			  },
			}
	    }).then((value) => {
			if (value) {
				callback(param)
			}
		});
	},
    fileUploadCustom: function(){
     //    $('.file-upload-browse').on('click', function() {
	    //   var file = $(this).parent().parent().parent().find('.file-upload-default');
	    //   file.trigger('click');
	    // });
	    $('.file-upload-default').on('change', function() {
	      $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
	    });
    },
    triggerFileUploadCustom: function(element){
        var file = element.parent().parent().parent().find('.file-upload-default');
        file.trigger('click');
        // console.log(file.val().replace(/C:\\fakepath\\/i, ''));
        // file.parent().find('.form-control').val(file.val().replace(/C:\\fakepath\\/i, ''));
    },
    testWhite:function(x){
        var white = new RegExp(/^\s$/);
        return white.test(x.charAt(0));
    },
    wordwrap: function(str, maxWidth) {
        var newLineStr = "\n"; done = false; res = '';
        while (str.length > maxWidth) {                 
            found = false;
            // Inserts new line at first whitespace of the line
            for (i = maxWidth - 1; i >= 0; i--) {
                if (appScript.testWhite(str.charAt(i))) {
                    res = res + [str.slice(0, i), newLineStr].join('');
                    str = str.slice(i + 1);
                    found = true;
                    break;
                }
            }
            // Inserts new line at maxWidth position, the word is too long to wrap
            if (!found) {
                res += [str.slice(0, maxWidth), newLineStr].join('');
                str = str.slice(maxWidth);
            }
       }
       return res + str;
    },
    createSelectOption: function (data) {
        var map = data.map(function(elem, index) {
            return '<option value="'+elem.id+'">'+elem.name+'</option>';
        })

        return map.join('');
    },
    createSelectOptionGrouped: function (data) {
        var html  = '';
        var label = '';
        $.each(data, function(index, val) {
            if (label != val.label) {
                if (label !='') {
                    html += '</optgroup>';
                }
                label = val.label;
                html += '<optgroup label="'+val.label+'">';
            }
            html += '<option value="'+val.id+'">'+val.name+'</option>';
        });
        if (html!='') {
            html += '</optgroup>';
        }
        return html;
    },
    setCookie: function(name, value, expire) {
        if (!expire) {
            var date = new Date();
            date.setTime(date.getTime() + (30 * 24 * 60 * 60 * 1000));
            expire = "; expires=" + date.toUTCString();
        }

        value = value || '';

        if (name) {
            document.cookie = name + "=" + value + expire + "; path=/";
        }
    },
    getCookie: function(name) {
        if (!name) {
            return null;
        }

        var find = name + "=";
        var cookies = document.cookie.split(';');

        for(var i = 0; i < cookies.length; i++) {
            var c = cookies[i];
            while (c.charAt(0)==' ') c = c.substring(1,c.length);
            if (c.indexOf(find) == 0) return c.substring(find.length,c.length);
        }
    },
};

$(document ).ajaxStart(function() {
    appScript.blockUI();
});

$(document ).ajaxStop(function() {
    appScript.unblockUI();
});
