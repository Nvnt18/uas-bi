<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

// Route::get('/', 'DashboardController@index')->name('dashboard');

// user harus login
Route::middleware(['auth'])->group(function() {

	//dicek pada hak akses group
	//route menu
	Route::middleware(['checkAccess'])->group(function() {
		
		Route::get('/', 'DashboardController@index')->name('dashboard');
		Route::get('apriori', 'AprioriController@index')->name('apriori');
		Route::get('pelanggan', 'PelangganController@index')->name('pelanggan');
		Route::get('keuangan', 'KeuanganController@index')->name('keuangan');
		Route::get('internal', 'InternalController@index')->name('internal');
		Route::get('growth', 'GrowthController@index')->name('growth');
		Route::get('target', 'TargetController@index')->name('target');

		Route::prefix('setting_hak_akses')->group(function () {
		    Route::get('/menu', 'SettingHakAkses\MenuController@index')->name('setting_hak_akses.menu');
		    Route::get('/user_group', 'SettingHakAkses\UserGroupController@index')->name('setting_hak_akses.user_group');
		    Route::get('/user', 'SettingHakAkses\UserController@index')->name('setting_hak_akses.user');
		});
	});

	//routing non-ajax
	Route::get('profile','ProfileController@index')->name('profile');

	//routing ajax
	Route::middleware(['isAjaxRequest']) ->group(function(){
		
    	Route::post('profile/save', 'ProfileController@save')->name('profile.save');

		Route::prefix('apriori')->group(function () {
			Route::get('/load_data','AprioriController@loadDataSetApriori')->name('apriori.load_data');
			Route::get('/check_persentase','AprioriController@checkPersentase')->name('apriori.check_persentase');
		});

		Route::prefix('pelanggan')->group(function () {
			Route::get('/load_data_orders','PelangganController@loadDataOrderPelanggan')->name('pelanggan.load_data_orders');
			Route::get('/load_data_comparison','PelangganController@loadDataComparisonChart')->name('pelanggan.load_data_comparison');
			Route::get('/load_data_on_time','PelangganController@loadOnTime')->name('pelanggan.load_data_on_time');
		});

		Route::prefix('keuangan')->group(function () {
			Route::get('/load_data_income','KeuanganController@loadIncome')->name('keuangan.load_data_income');
			Route::get('/load_data_net_profit','KeuanganController@loadNetProfit')->name('keuangan.load_data_net_profit');
			Route::get('/load_data_orders','KeuanganController@loadOrders')->name('keuangan.load_data_orders');
		});

		Route::prefix('internal')->group(function () {
			Route::get('/load_data_office','InternalController@loadOffice')->name('internal.load_data_office');
			Route::get('/load_data_product','InternalController@loadProduct')->name('internal.load_data_product');
			Route::get('/load_data_status','InternalController@loadStatus')->name('internal.load_data_status');
		});

		Route::prefix('growth')->group(function () {
			Route::get('/load_data','GrowthController@loadData')->name('growth.load_data');
			Route::get('/load_data_sales','GrowthController@loadSales')->name('growth.load_data_sales');
		});

		Route::prefix('target')->group(function () {
			Route::get('/load_data','TargetController@loadData')->name('target.load_data');
			Route::get('/show','TargetController@show')->name('target.show');

			Route::post('/save','TargetController@save')->name('target.save');
			Route::post('/delete','TargetController@delete')->name('target.delete');
			Route::post('/delete_batch','TargetController@deleteBatch')->name('target.delete_batch');
		});

		Route::prefix('menu')->group(function () {
			Route::get('/load_data','SettingHakAkses\MenuController@loadData')->name('menu.load_data');
			Route::get('/show','SettingHakAkses\MenuController@show')->name('menu.show');

			Route::post('/save','SettingHakAkses\MenuController@save')->name('menu.save');
			Route::post('/update_menu_structure','SettingHakAkses\MenuController@updateMenuStructure')->name('menu.update_menu_structure');
			Route::post('/delete','SettingHakAkses\MenuController@delete')->name('menu.delete');
		});

		Route::prefix('user_group')->group(function () {
			Route::get('/load_data','SettingHakAkses\UserGroupController@loadData')->name('user_group.load_data');
			Route::get('/load_group_access_menu','SettingHakAkses\UserGroupController@loadGroupAccessMenu')->name('user_group.load_group_access_menu');
			Route::get('/show','SettingHakAkses\UserGroupController@show')->name('user_group.show');
			Route::get('/combobox','SettingHakAkses\UserGroupController@combobox')->name('user_group.combobox');

			Route::post('/save','SettingHakAkses\UserGroupController@save')->name('user_group.save');
			Route::post('/save_group_access','SettingHakAkses\UserGroupController@saveGroupAccess')->name('user_group.save_group_access');
			Route::post('/delete','SettingHakAkses\UserGroupController@delete')->name('user_group.delete');
			Route::post('/delete_batch','SettingHakAkses\UserGroupController@deleteBatch')->name('user_group.delete_batch');
		});

		Route::prefix('user')->group(function () {
			Route::get('/load_data','SettingHakAkses\UserController@loadData')->name('user.load_data');
			Route::get('/show','SettingHakAkses\UserController@show')->name('user.show');
			Route::get('/check_email','SettingHakAkses\UserController@checkEmailAvailable')->name('user.check_email');

			Route::post('/save','SettingHakAkses\UserController@save')->name('user.save');
			Route::post('/delete','SettingHakAkses\UserController@delete')->name('user.delete');
			Route::post('/delete_batch','SettingHakAkses\UserController@deleteBatch')->name('user.delete_batch');
			Route::post('/update_status_batch','SettingHakAkses\UserController@updateStatusBatch')->name('user.update_status_batch');
		});		
	});

});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
