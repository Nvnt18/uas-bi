<?php

use Illuminate\Database\Seeder;
use App\Models\Menu;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$urutanMenu = 0;
		
		$menu = new Menu;
		$menu->menu_name        = 'Home';
		$menu->menu_description = 'Halaman Dashboard Perusahaan';
		$menu->menu_icon        = 'fa fa-home';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = '/';
		$menu->menu_parent_id   = null;
		$menu->created_by       = 'system';
		$menu->save();

    	$this->dashboardMenu($urutanMenu++);

		$menu = new Menu;
		$menu->menu_name        = 'Target';
		$menu->menu_description = 'Halaman Setting Target';
		$menu->menu_icon        = 'fa fa-bullseye';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'target';
		$menu->menu_parent_id   = null;
		$menu->created_by       = 'system';
		$menu->save();

		$this->settingHakAksesMenu($urutanMenu++);
	}

	private function dashboardMenu($urutanParent)
    {
    	$urutanMenu = 0;

    	$menu = new Menu;
		$menu->menu_name        = 'Jenis Dashboard';
		$menu->menu_description = 'Halaman Dashboard Perusahaan';
		$menu->menu_icon        = 'fa fa-chart-bar';
		$menu->menu_sequence    = $urutanParent;
		$menu->menu_url         = '#';
		$menu->menu_parent_id   = null;
		$menu->created_by       = 'system';
		$menu->save();

		$menuParentId = $menu->menu_id;

		$menu = new Menu;
		$menu->menu_name        = 'Pelanggan';
		$menu->menu_description = 'Halaman Dashboard Perspektif Pelanggan';
		$menu->menu_icon        = 'fa fa-users';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'pelanggan';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';
		$menu->save();

		$menu = new Menu;
		$menu->menu_name        = 'Keuangan';
		$menu->menu_description = 'Halaman Dashboard Perspektif Keuangan';
		$menu->menu_icon        = 'fa fa-credit-card';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'keuangan';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';
		$menu->save();

		$menu = new Menu;
		$menu->menu_name        = 'Internal';
		$menu->menu_description = 'Halaman Dashboard Perspektif Internal';
		$menu->menu_icon        = 'fa fa-building';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'internal';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';    	
		$menu->save();

		$menu = new Menu;
		$menu->menu_name        = 'Tumbuh & Berkembang';
		$menu->menu_description = 'Halaman Dashboard Perspektif Tumbuh dan Berkembang';
		$menu->menu_icon        = 'fa fa-paper-plane';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'growth';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';    	
		$menu->save();

		
		$menu = new Menu;
		$menu->menu_name        = 'Apriori';
		$menu->menu_description = 'Halaman Dashboard Metode Apriori';
		$menu->menu_icon        = 'fa fa-code';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'apriori';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';    	
		$menu->save();
    }

    private function settingHakAksesMenu($urutanParent)
    {
    	$urutanMenu = 0;

    	$menu = new Menu;
		$menu->menu_name        = 'Setting Hak Akses';
		$menu->menu_description = null;
		$menu->menu_icon        = 'fa fa-key';
		$menu->menu_sequence    = $urutanParent;
		$menu->menu_url         = '#';
		$menu->menu_parent_id   = null;
		$menu->created_by       = 'system';
		$menu->save();

		$menuParentId = $menu->menu_id;

		$menu = new Menu;
		$menu->menu_name        = 'Menu';
		$menu->menu_description = 'Halaman Kelola Menu';
		$menu->menu_icon        = 'fa fa-list';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'setting_hak_akses/menu';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';
		$menu->save();

		$menu = new Menu;
		$menu->menu_name        = 'User Group';
		$menu->menu_description = 'Halaman Kelola User Group';
		$menu->menu_icon        = 'fa fa-users';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'setting_hak_akses/user_group';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';
		$menu->save();

		$menu = new Menu;
		$menu->menu_name        = 'User';
		$menu->menu_description = 'Halaman Kelola User';
		$menu->menu_icon        = 'fa fa-users-cog';
		$menu->menu_sequence    = $urutanMenu++;
		$menu->menu_url         = 'setting_hak_akses/user';
		$menu->menu_parent_id   = $menuParentId;
		$menu->created_by       = 'system';    	
		$menu->save();
    }
}
