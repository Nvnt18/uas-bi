<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "user_group_id"     => 1,
            "name"              => 'Super Administrator',
            "email"             => 'superadmin@uas-bi',
            "email_verified_at" => now(),
            "password"          => '123456',
            "status"            => 'A',
            "type"              => 'R',
            "created_by"        => 'system'
        ]);
    }
}
