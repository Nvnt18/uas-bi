<?php

use Illuminate\Database\Seeder;
use App\Models\UserGroup;

class UserGroupAccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userGroup = UserGroup::find(1);

        $menuId = \App\Models\Menu::all()->map(function($item){
            return $item->menu_id;
        });
        
        $userGroup->menus()->sync($menuId);
    }
}
