<?php

use Illuminate\Database\Seeder;
use App\Models\UserGroup;

class UserGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserGroup::create([
            'user_group_id'   => 1,
            'user_group_name' => 'Superadmin',
            "user_group_type" => 'R',
            'created_by'      => 'system'
        ]);
    }
}
