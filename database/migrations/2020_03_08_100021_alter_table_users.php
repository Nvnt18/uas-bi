<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->bigInteger('user_group_id')->unsigned()->after('id')->nullable();
            $table->enum('status',['A','T'])->after('remember_token')->default('A')->comment = 'A: Aktif, T:Tidak Aktif';
            $table->enum('type',['R','C'])->after('status')->default('C')->comment = 'R: Root User, C:Common User';
            $table->string('avatar',100)->after('type')->nullable();
            $table->string('created_by',100)->after('updated_at');
            $table->string('updated_by',100)->nullable();

            //create foreign key constraint
            $table->foreign('user_group_id')->references('user_group_id')->on('user_groups')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn('user_group_id');
            $table->dropColumn('status');
            $table->dropColumn('created_by');
            $table->dropColumn('updated_by');
        });
    }
}
