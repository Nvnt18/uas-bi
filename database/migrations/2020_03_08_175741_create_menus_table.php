<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->id('menu_id');
            $table->string('menu_name',100);
            $table->string('menu_description')->nullable();
            $table->string('menu_url',100);
            $table->string('menu_icon',100)->nullable();
            $table->integer('menu_sequence');
            $table->bigInteger('menu_parent_id')->unsigned()->nullable();
            $table->timestamps();
            $table->string('created_by',100);
            $table->string('updated_by',100)->nullable();
        });

        Schema::table('menus', function(Blueprint $table) {
            $table->foreign('menu_parent_id')->references('menu_id')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
