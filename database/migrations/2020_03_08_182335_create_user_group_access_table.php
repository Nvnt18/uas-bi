<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserGroupAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_group_access', function (Blueprint $table) {
            $table->bigInteger('menu_id')->unsigned();
            $table->bigInteger('user_group_id')->unsigned();

            $table->primary(['menu_id', 'user_group_id']);
            $table->foreign('menu_id')->references('menu_id')->on('menus')->onUpdate('cascade');
            $table->foreign('user_group_id')->references('user_group_id')->on('user_groups')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_group_access');
    }
}
