<?php 

	return [
		'simpan_icon'     => 'fa fa-save',
		'simpan'          => 'Simpan',
		'batal'           => 'Batal',
		'batal_icon'      => 'fa fa-times',
		'muat_ulang'      => 'Muat Ulang',
		'muat_ulang_icon' => 'fa fa-sync',
		'ubah'            => "Ubah",
		'ubah_icon'       => "fa fa-edit",
		'hapus'           => "Hapus",
		'hapus_icon'      => "fa fa-trash",
		'cari'            => "Cari",
		'cari_hint'       => "Cari ...",
		'cari_icon'       => "fa fa-search",
		'reset'           => 'Reset',
		'reset_icon'      => 'fa fa-undo',
		'tambah'          => 'Tambah',
		'tambah_icon'     => 'fa fa-plus-square',
	];
 ?>