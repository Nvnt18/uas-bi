<?php

return [
	'ajax_error'                  => 'Terjadi kesalahan di server. Harap coba beberapa saat lagi.',
	'data_not_found'              => 'Data tidak ditemukan.',
	'delete_confirm'              => 'Apakah anda yakin menghapus data ini?',
	'delete_batch_confirm'        => 'Apakah anda yakin menghapus data yang dicentang?',
	'update_batch_confirm'        => 'Apakah anda yakin mengubah data yang dicentang?',
	'update_status_batch_confirm' => 'Apakah anda yakin mengubah status data yang dicentang?',
	'confirm'                     => 'Konfirmasi',
	'save_data_success'           => 'Berhasil menyimpan data',
	'save_data_failed'            => 'Gagal menyimpan data',
	'create_data_success'         => 'Berhasil menambah data',
	'create_data_failed'          => 'Gagal menambah data',
	'update_data_success'         => 'Berhasil memperbarui data',
	'update_data_failed'          => 'Gagal memperbarui data',
	'delete_data_success'         => 'Berhasil menghapus data',
	'delete_data_failed'          => 'Gagal menghapus data',
];
