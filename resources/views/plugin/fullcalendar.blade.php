@push('style')
<link rel="stylesheet" href="{{ asset('assets/plugins/fullcalendar/dist/fullcalendar.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('assets/plugins/fullcalendar/dist/fullcalendar.min.js') }}"></script>
@endpush