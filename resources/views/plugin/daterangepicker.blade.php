@push('style')
<link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
@endpush

@push('script')
<script src="{{ asset('assets/plugins/daterangepicker/daterangepicker.js') }}"></script>
@endpush