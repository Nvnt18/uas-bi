@push('style')
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-nestable/jquery.nestable.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('assets/plugins/jquery-nestable/jquery.nestable.min.js') }}"></script>
@endpush