@push('style')
<link rel="stylesheet" href="{{ asset('assets/plugins/select2/dist/css/select2.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('assets/plugins/select2/dist/js/select2.min.js') }}"></script>
@endpush