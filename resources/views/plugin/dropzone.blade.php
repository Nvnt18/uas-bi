@push('style')
{{-- <link rel="stylesheet" href="{{ asset('assets/plugins/dropzone/min/basic.min.css') }}"> --}}
<link rel="stylesheet" href="{{ asset('assets/plugins/dropzone/min/dropzone.min.css') }}">
@endpush

@push('script')
<script src="{{ asset('assets/plugins/dropzone/localization/messages_id.min.js') }}"></script>
<script src="{{ asset('assets/plugins/dropzone/min/dropzone.min.js') }}"></script>
@endpush