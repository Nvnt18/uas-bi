@push('style')
<style type="text/css">
    .highcharts-figure{
        align: center;
    }

    .highcharts-figure, .highcharts-data-table table {
        width: 100%;
        margin: 1em auto;
    }

    .highcharts-figure .chart-container {
        width: 100%;
        height: 200px;
        float: left;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #EBEBEB;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }
    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }
    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }
    .highcharts-data-table td, .highcharts-data-table th, .highcharts-data-table caption {
        padding: 0.5em;
    }
    .highcharts-data-table thead tr, .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }
    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
    .buttons {
        min-width: 310px;
        text-align: center;
        margin-bottom: 1.5rem;
        font-size: 0;
    }

    .buttons button {
        cursor: pointer;
        border: 1px solid silver;
        border-right-width: 0;
        background-color: #f8f8f8;
        font-size: 1rem;
        padding: 0.5rem;
        outline: none;
        transition-duration: 0.3s;
    }

    .buttons button:first-child {
        border-top-left-radius: 0.3em;
        border-bottom-left-radius: 0.3em;
    }

    .buttons button:last-child {
        border-top-right-radius: 0.3em;
        border-bottom-right-radius: 0.3em;
        border-right-width: 1px;
    }

    .buttons button:hover {
        color: white;
        background-color: rgb(158, 159, 163);
        outline: none;
    }

    .buttons button.active {
        background-color: #0051B4;
        color: white;
    }

    @media (max-width: 600px) {
        .highcharts-figure, .highcharts-data-table table {
            width: 100%;
        }
        .highcharts-figure .chart-container {
            width: 300px;
            float: none;
            margin: 0 auto;
        }
    }
</style>
@endpush

@push('script')
<script src="{{ asset('assets/plugins/highcharts/code/highcharts.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/highcharts-more.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/solid-gauge.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/series-label.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/drilldown.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/exporting.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/export-data.js') }}"></script>
<script src="{{ asset('assets/plugins/highcharts/code/modules/accessibility.js') }}"></script>
@endpush
