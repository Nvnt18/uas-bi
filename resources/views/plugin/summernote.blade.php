@push('style')
<link rel="stylesheet" href="{{ asset('assets/plugins/summernote/dist/summernote-bs4.css') }}">
@endpush
@push('script')
<script src="{{ asset('assets/plugins/summernote/dist/summernote-bs4.min.js') }}"></script>
@endpush