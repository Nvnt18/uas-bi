@extends('layouts.auth')

@section('content')
<div class="authentication-form mx-auto">
    <div class="logo-centered">
     <a href="{{ url('/') }}" ><img src="{{ asset('assets/img/icon.png') }}" alt="" class="img img-responsive img-thumbnail"></a>
    </div>
    <h3>{{ config('app.name', 'Laravel') }} | Reset Password</h3>
    <!-- <p>Selamat Datang Kembali!</p> -->
    <form action="{{ route('password.update') }}" method="POST" id="form">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid text-red @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail">
            <i class="ik ik-mail"></i>
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid text-red @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            <i class="ik ik-lock"></i>
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Konfirmasi Password">
            <i class="ik ik-lock"></i>
            <span class="help-block"></span>
        </div>
        @error('email')
            <div class="alert alert-danger alert-dismissible fade show mt-10" role="alert" id="failed-message">
                <strong>Gagal!</strong> {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="ik ik-x"></i>
                </button>
            </div>
        @enderror
        <div class="sign-btn text-center">
            <button class="btn btn-success btn-block" type="submit">Reset Password</button>
        </div>
    </form>
</div>

@endsection


@push('js_bottom')

<script type="text/javascript">
    $(document).ready(function() {
        $('#form').validate({
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                element.parent("div").find(".help-block").empty();
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        setTimeout(function() {
            $('#failed-message').fadeOut('slow');}, 3000
        );

    });
</script>

@endpush


