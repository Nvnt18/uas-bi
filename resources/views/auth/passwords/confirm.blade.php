@extends('layouts.auth')

@section('content')

<div class="authentication-form mx-auto">
    <div class="logo-centered">
     <a href="{{ url('/') }}" ><img src="{{ asset('assets/img/icon.png') }}" alt="" class="img img-responsive img-thumbnail"></a>
    </div>
    <h3>{{ config('app.name', 'Laravel') }} | Konfirmasi Ulang Password</h3>
    <p>Harap Konfirmasi Password Anda Sebelum Melanjutkan!</p>
    <form action="{{ route('password.confirm') }}" method="POST" id="form_login">
        @csrf
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid text-red @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            <i class="ik ik-mail"></i>
            <span class="help-block"></span>
        </div>
        <div class="row">
            @if (Route::has('password.request'))
             <div class="col text-right">
                <a href="{{ route('password.request') }}">Lupa Password ?</a>
            </div>
            @endif
        </div>
        @error('password')
            <div class="alert alert-danger alert-dismissible fade show mt-10" role="alert" id="failed-message">
                <strong>Gagal!</strong> {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="ik ik-x"></i>
                </button>
            </div>
        @enderror
        <div class="sign-btn text-center">
            <button class="btn btn-success btn-block" type="submit">Konfirmasi Password</button>
        </div>
    </form>
</div>

@endsection

@push('js_bottom')

<script type="text/javascript">
    $(document).ready(function() {
        $('#form').validate({
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.insertAfter(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        setTimeout(function() {
            $('#failed-message').fadeOut('slow');}, 3000
        );
    });
</script>

@endpush

