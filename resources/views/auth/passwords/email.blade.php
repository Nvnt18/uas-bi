@extends('layouts.auth')

@section('content')

<div class="authentication-form mx-auto">
    <div class="logo-centered">
     <a href="{{ url('/') }}" ><img src="{{ asset('assets/img/icon.png') }}" alt="" class="img img-responsive img-thumbnail"></a>
    </div>
    <h3>{{ config('app.name', 'Laravel') }} | Lupa Password</h3>
    <p>Masukkan Email dan Kami akan mengirimkan link untuk reset password!</p>
    <form action="{{ route('password.email') }}" method="POST" id="form_login">
        @csrf
        <div class="form-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail">
            <i class="ik ik-mail"></i>
            <span class="help-block"></span>
        </div>
        <div class="register">
            <p>Kembali ke halaman <a href="{{ route('login') }}" class="font-weight-bold">Login</a></p>
        </div>
        @error('email')
            <div class="alert alert-danger alert-dismissible fade show mt-10" role="alert" id="failed-message">
                <strong>Gagal!</strong> {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="ik ik-x"></i>
                </button>
            </div>
        @enderror
        <div class="sign-btn text-center">
            <button class="btn btn-success btn-block" type="submit">Kirim</button>
        </div>
    </form>
</div>

@endsection

@push('js_bottom')

<script type="text/javascript">
    $(document).ready(function() {
        $('#form').validate({
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.insertAfter(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        setTimeout(function() {
            $('#failed-message').fadeOut('slow');}, 3000
        );
    });
</script>

@endpush
