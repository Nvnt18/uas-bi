@extends('layouts.auth')

@section('content')
<div class="authentication-form mx-auto">
    <div class="logo-centered" style="height:135px; width:auto;">
     <a href="{{ url('/') }}" ><img src="{{ asset('assets/img/icon.png') }}" alt="" class="img img-responsive img-thumbnail"></a>
    </div>
    <h3>{{ config('app.name', 'Laravel') }} | Login</h3>
    <!-- <p>Selamat Datang Kembali!</p> -->
    <form action="{{ route('login') }}" method="POST" id="form">
        @csrf
        <div class="form-group">
            <input id="email" type="email" class="form-control @error('email') is-invalid text-red @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Username" autofocus>
            <i class="ik ik-mail"></i>
            <span class="help-block"></span>
        </div>
        <div class="form-group">
            <input id="password" type="password" class="form-control @error('password') is-invalid text-red @enderror" name="password" required autocomplete="current-password" placeholder="Password">
            <i class="ik ik-lock"></i>
            <span class="help-block"></span>
        </div>
        <div class="row">
            <div class="col text-left">
                <label class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <span class="custom-control-label">&nbsp;Ingat Saya</span>
                </label>
            </div>
             <!-- <div class="col text-right">
                <a href="{{ route('password.request') }}">Lupa Password ?</a>
            </div> -->
        </div>
        @error('email')
            <div class="alert alert-danger alert-dismissible fade show mt-10" role="alert" id="failed-message">
                <strong>Gagal!</strong> {{ $message }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <i class="ik ik-x"></i>
                </button>
            </div>
        @enderror
        <div class="sign-btn text-center">
            <button class="btn btn-success btn-block" type="submit">Login</button>
        </div>
    </form>
</div>

@endsection


@push('js_bottom')

<script type="text/javascript">
    $(document).ready(function() {
        $('#form').validate({
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                element.parent("div").find(".help-block").empty();
                error.appendTo(element.parent("div").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });

        setTimeout(function() {
            $('#failed-message').fadeOut('slow');}, 3000
        );

    });
</script>

@endpush


