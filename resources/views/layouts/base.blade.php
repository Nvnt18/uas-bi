<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <!-- CSRF Token -->
	    <meta name="csrf-token" content="{{ csrf_token() }}">
	    <title></title>
        <meta name="description" content="Halaman Administrator KRI I Gusti Ngurah Rai">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{{ asset('assets/icon.ico') }}" type="image/x-icon" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:300,400,600,700,800">
        <link rel="stylesheet" href="{{ asset('assets/plugins/pace/pace-theme-flash.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/icon-kit/dist/css/iconkit.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/ionicons/dist/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css') }}">
        @stack('style')
        <link rel="stylesheet" href="{{ asset('assets/dist/css/theme.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/dist/css/style.css') }}">
        @stack('page_style')
        <script src="{{ asset('assets/src/js/vendor/modernizr-2.8.3.min.js') }}"></script>
	</head>

    <body>
        <div class="wrapper">
            <x-header/>

            <div class="page-wrap">
                <div class="app-sidebar colored">
                    <div class="sidebar-header">
                        <a class="header-brand" href="">
                            <div class="logo-img">
                               <img src="{{ asset('assets/img/logo_image.png') }}" class="header-brand-img" alt="lavalite" style="width:50px !important;background: transparant; padding: 2px; border-radius: 7px"> 
                            </div>
                            <span class="text" style="font-size: 18px">&nbsp; &nbsp; {{ config('app.name', 'Laravel') }}</span>
                        </a>
                        <button id="sidebarClose" class="nav-close"><i class="ik ik-x"></i></button>
                    </div>
                    
                    <x-sidebar :current-page="Request::path()"/>
                </div>
                <div class="main-content">
                    <div class="container-fluid">
                        <x-page-header :current-page="Request::path()"/>
                        
                        @yield('content')
                     </div>
                </div>

                <footer class="footer">
                    <div class="w-100 clearfix">
                        <span class="text-center text-sm-left d-md-inline-block">Copyright © {{ date('Y') == '2020' ? '2020' : '2020-'.date('Y') }}</span>
                        <span class="float-none float-sm-right mt-1 mt-sm-0 text-center">Developed by <b>Miniature Company</b></a></span>
                    </div>
                </footer>
                
            </div>
        </div>
        
        <script src="{{ asset('assets/src/js/vendor/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/moment/moment-with-locales.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/pace/pace.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/popper.js/dist/umd/popper.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/screenfull/dist/screenfull.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery.blockui.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/sweetalert/sweetalert.min.js') }}"></script>
        <script src="{{ asset('assets/plugins/bootbox/bootbox.all.min.js') }}"></script>
        @stack('script')
        <script src="{{ asset('assets/dist/js/theme.js') }}"></script>
        <script src="{{ asset('assets/js/appscript.js') }}"></script>
        <script type="text/javascript">moment.locale('{{ app()->getLocale() }}')</script>
        @stack('page_script')
    </body>
</html>
