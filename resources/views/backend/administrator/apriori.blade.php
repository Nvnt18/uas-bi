@extends('layouts.base')

@include('plugin.jquery-validation')
@include('plugin.highchart')
@include('plugin.datatable')
@include('plugin.datepicker')
@include('plugin.autonumeric')
@include('plugin.amchart')

@section('content')

<div class="card">
    <div class="card-header row">
        <div class="col-md-1">
            <div class="card-search with-adv-search dropdown">
                Supp. %
            </div>
        </div>
        <div class="col-md-2">
            <div class="card-search with-adv-search dropdown">
                <input type="text" id="fl_support" name="fl_support" class="form-control autonumeric" placeholder="Supp. (%)" value="30" required>
            </div>
        </div>
        <div class="col-md-1">
            <div class="card-search with-adv-search dropdown">
                Conf. %
            </div>
        </div>
        <div class="col-md-2">
            <div class="card-search with-adv-search dropdown">
                <input type="text" id="fl_confidence" name="fl_confidence" class="form-control autonumeric" placeholder="Conf. (%)" value="30" required>
            </div>
        </div>
        <div class="col col-md-1">
            <div class="card-search with-adv-search dropdown">
                <button class="btn btn-outline-dark" type="button" onclick="reloadTable()"><i class="{{ __('label.cari_icon') }}"></i>{{ __('label.cari') }}</button>
            </div>
        </div>
        <div class="col col-md-1">
            <button class="btn btn-outline-dark" type="button" onclick="reloadTable()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
        </div>
        <div class="col col-md-4">
            <div class="card-options text-right" id="length_dropdown">
                <span class="text-muted text-small mr-2">Tampilkan: </span>
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="current_length">Semua</span><i class="ik ik-chevron-down mr-0 align-middle"></i>
                </button>
                data
                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1617px, 30px, 0px);">
                    <a class="dropdown-item length_option" data-value="All" data-label="Semua" href="javascript:;">Semua</a>
                    <a class="dropdown-item length_option" data-value="10" data-label="10" href="javascript:;">10</a>
                    <a class="dropdown-item length_option" data-value="20" data-label="25" href="javascript:;">25</a>
                    <a class="dropdown-item length_option" data-value="50" data-label="50" href="javascript:;">50</a>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <figure class="highcharts-figure">
            <div id="container"></div>
        </figure>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" id="tabel_card">
        <form class="forms-sample" id="form" action="javascript:;">
            <div class="card-body">
                <div class="dt-responsive">
                    <table id="tabel" class="table table-inverse table-hover borderless" width="100%">
                        <thead>
                            <tr>
                                <th>Item Set</th>
                                <th>Support</th>
                                <th>Confidence</th>
                                <th>Indicators</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('script')
<script type="text/javascript">
    
    $(document).ready(function() {

        appScript.autoNumericPercentage();

        loadNegativeChart();

        $('#tabel').DataTable({
            scrollCollapse: true,
            dom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            iDisplayLength: -1,
            scrollX:true,
            ajax:{
                url : "{{ route('apriori.load_data') }}",
                type : 'GET',
                data : function(d){
                    d.support = $('#fl_support').val();
                    d.confidence = $('#fl_confidence').val();
                },
            },
            language: {
                url: datatableLang
            },
            columnDefs: [
                {visible: false, targets : []},
                {orderable: false, targets : ["column-checkbox",-1]},
            ],
            order: [[1,"desc"],[2,"desc"]],
            columns: [
                { width: "55%", data: "item"},
                { width: "15%", data: "support"},
                { width: "15%", data: "confidence"},
                { width: "15%", data: "indicators"}
            ],
            drawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#length_dropdown').on('click', '.length_option', function(event) {
            var value = $(this).data('value');
            var label = $(this).data('label');

            $('#length_dropdown').find('.current_length').text(label);

            var t = $('#tabel').DataTable();
            var length = value == 'All' ? -1 : value;
            t.page.len(length).draw();
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.DataTable().search($(this).val()).draw();
        });

        $("#fl_support").on('keyup', function (e) {
            if (e.keyCode == 13) {
                reloadTable();
            }
        });

        $("#fl_confidence").on('keyup', function (e) {
            if (e.keyCode == 13) {
                reloadTable();
            }
        });
    });

    function reloadTable() {
        loadNegativeChart();
        var t = $('#tabel').DataTable();
        t.ajax.reload();
    }

    function loadNegativeChart(){
        $.ajax({
            url : "{{ route('apriori.load_data') }}",
            type : 'GET',
            data : {
                support : $('#fl_support').val(),
                confidence : $('#fl_confidence').val()
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    negativeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function negativeChart(data) {
        Highcharts.chart('container', {
            chart: {
                type: 'bar',
                height: (19 + (data.size*3)) + '%'
            },
            scrollbar: {
                enabled: true
            },
            title: {
                text: 'Market Based Analysis'
            },
            subtitle: {
                text: 'Apriori algorithm: Powered By <a target="_blank" href="https://php-ml.readthedocs.io/en/latest/">PHP Machine Learning</a>'
            },
            accessibility: {
                point: {
                    valueDescriptionFormat: '{index}. Age {xDescription}, {value}%.'
                }
            },
            xAxis: [{
                categories: data.dataAntecedent,
                reversed: false,
                labels: {
                    step: 1
                },
                accessibility: {
                    description: 'Age (male)'
                }
            }, { // mirror axis on right side
                opposite: true,
                reversed: false,
                categories: data.dataConsequent,
                linkedTo: 0,
                labels: {
                    step: 1
                },
                accessibility: {
                    description: 'Age (female)'
                }
            }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return Math.abs(this.value) + '%';
                    }
                },
                accessibility: {
                    description: 'Percentage population',
                    rangeDescription: 'Range: 0 to 5%'
                }
            },

            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },

            tooltip: {
                formatter: function () {
                    return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                        'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 1) + '%';
                }
            },

            series: [{
                name: 'Support',
                data: data.dataSupport,
                color: '#0f46bd'
            }, {
                name: 'Confidence',
                data: data.dataConfidence,
                color: '#099e31'
            }]
        });
    }
</script>
@endpush