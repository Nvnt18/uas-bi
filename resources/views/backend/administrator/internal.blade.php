@extends('layouts.base')

@include('plugin.highchart')
@include('plugin.datatable')
@include('plugin.datepicker')
@include('plugin.amchart')

@section('content')

<div class="card home_card">
    <div class="card-header row">
        <div class="col-md-7">
            <div class="input-group">
                <h3>Balanced Scorecard <b>Perspektif Internal</b></h3>
            </div>
        </div>
        <div class="col col-md-2">
            <div class="input-group input-group-inverse">
                <button class="btn btn-outline-dark" type="button" onclick="reloadChart()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control tahun" value="{{ date('Y') }}" name="fl_date" data-target="#fl_date" id="fl_date" placeholder="Tahun">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 id="status_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="status_chart"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 id="on_time_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="on_time_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<div class="row" style="display:none">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 id="office_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="office_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<div class="row" style="display:none">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 id="product_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="product_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
    
    $(document).ready(function() {
        appScript.yearPicker();	

        getYear($('#fl_date').val());
        loadOnTimeChart();
        loadStatusChart();

        $('#fl_date').on('change.datetimepicker', function(event) {
            reloadChart();
            
            getYear($('#fl_date').val());
        });
    });

    function reloadChart(){
        loadOnTimeChart();
        loadStatusChart();
    }

    function getYear(date){
        var tahun = (date ? moment(date,'YYYY').format('YYYY') : '');

        $('#on_time_text').text('Ketepatan Pengiriman Pesanan Tahun '+tahun);
        $('#status_text').text('Status Pesanan Tahun '+tahun);
    }

    function getMonth(date){
        var bulan = (date ? moment(date,'MM-YYYY').format('MMMM') : '');

        $('#office_text').text('Penjualan tiap Cabang bulan '+bulan);
        $('#product_text').text('Penjualan tiap Product bulan '+bulan);
    }

    function loadOnTimeChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_on_time') }}",
            type : 'GET',
            data: {
                date: $('#fl_date').val()
            },
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    onTimeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadStatusChart(){
        $.ajax({
            url : "{{ route('internal.load_data_status') }}",
            type : 'GET',
            data: {
                date: $('#fl_date').val()
            },
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    statusChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadOfficeChart(){
        $.ajax({
            url : "{{ route('internal.load_data_office') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    officeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadProductChart(){
        $.ajax({
            url : "{{ route('internal.load_data_product') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    productChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function onTimeChart(data){
        Highcharts.chart('on_time_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: data.status
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} Orders)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Ketepatan',
                colorByPoint: true,
                data: [{
                    name: 'Tepat Waktu',
                    y: data.on_time,
                    color: '#2dce89'
                }, {
                    name: 'Terlambat',
                    y: data.late,
                    color: '#f5365c'
                }]
            }]
        });
    }

    function statusChart(data){
        Highcharts.chart('status_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: data.status
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} Orders)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Status',
                colorByPoint: true,
                data: data.data
            }]
        });
    }

    function officeChart(data){
        Highcharts.chart('office_chart', {
            chart: {
                type: 'column'
            },
            title: false,
            xAxis: {
                categories: data.categories,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Jumlah'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Jumlah Penjualan',
                data: data.data

            }]
        });
    }

    function productChart(data){
        console.log(data.productlines);
        console.log(data.drilldown);
        Highcharts.chart('product_chart', {
            chart: {
                type: 'pie'
            },
            title: false,
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
            },

            series: [
                {
                    name: "Browsers",
                    colorByPoint: true,
                    data: data.productlines
                }
            ],
            drilldown: {
                series: data.drilldown
            }
        });
    }

</script>
@endpush