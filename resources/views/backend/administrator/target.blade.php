@extends('layouts.base')

@include('plugin.jquery-validation')
@include('plugin.datatable')
@include('plugin.select2')
@include('plugin.autonumeric')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card" id="tabel_card">
            <div class="card-header row no-gutters">
                <div class="col col-md-2">
                    <button class="btn btn-primary btn-block" type="button" id="btnAdd"><i class="{{ __('label.tambah_icon') }}"></i> Data</button>
                </div>
                <div class="col col-md-6">
                    <div class="card-search with-adv-search dropdown">
                        <form action="javascript:;">
                            <input type="text" id="input_pencarian" autofocus class="form-control" placeholder="{{ __('label.cari_hint') }}" required="">
                            <button type="button" class="btn btn-icon"><i class="{{ __('label.cari_icon') }}"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col col-md-1">
                    <button class="btn btn-outline-dark" type="button" onclick="reloadTable()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
                </div>
                <div class="col col-md-3">
                    <div class="card-options text-right" id="length_dropdown">
                        <span class="text-muted text-small mr-2">Tampilkan: </span>
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="current_length">10</span><i class="ik ik-chevron-down mr-0 align-middle"></i>
                        </button>
                        data
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1617px, 30px, 0px);">
                            <a class="dropdown-item length_option" data-value="10" data-label="10" href="javascript:;">10</a>
                            <a class="dropdown-item length_option" data-value="20" data-label="25" href="javascript:;">25</a>
                            <a class="dropdown-item length_option" data-value="50" data-label="50" href="javascript:;">50</a>
                            <a class="dropdown-item length_option" data-value="All" data-label="Semua" href="javascript:;">Semua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="dt-responsive">
                    <table id="tabel" class="table table-inverse table-hover" width="100%">
                        <thead>
                            <tr>
                                <th class="column-checkbox"><label class="custom-control custom-checkbox m-0">
                                        <input type="checkbox" class="custom-control-input" id="select_all" name="select_all" value="">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </th>
                                <th>No.</th>
                                <th>Keterangan</th>
                                <th>Target</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row" id="aksi_lain">
                    <div class="col col-md-2">
                        <button type="button" class="btn btn-danger btn-block" id="btnHapus" disabled><span class="{{ __('label.hapus_icon') }}"></span> {{ __('label.hapus') }}</button>
                    </div>
                    <div class="col col-md-10">
                        <span class="text-muted text-small mr-2 pt-5 float-right"><b id="data_terpilih">0</b> Data Terpilih</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" id="form_card" style="display: none">
            <form class="forms-sample" id="form" method="POST" action="javascript:;">	
            <div class="card-header"><h6 class="font-weight-bold">Form</h6></div>
            <div class="card-body">
                @csrf
                <input type="hidden" name="id_target" id="id_target">
                <div class="form-group">
                    <label for="keterangan">Kategori <span class="text-red">*</span></label>
                    <select name="keterangan" id="keterangan" class="form-control cmb_select2" required="required">
                        <option value="Target 1">Income</option>
                        <option value="Target 2">Net Profit</option>
                        <option value="Target 3">Status Kredit</option>
                        <option value="Target 4">Penjualan Karyawan</option>
                        <option value="Target 5">Target Pelanggan</option>
                        <option value="Target 6">On Time Delivery</option>
                        <option value="Target 7">Status Pesanan</option>
                    </select>
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label for="target">Target <span class="text-red">*</span></label>
                    <input type="text" class="form-control autonumeric" name="target" id="target" required>
                    <span class="help-block"></span>
                </div>
            </div>
            <div class="card-footer">
                <button id="btnSimpan" type="submit" class="btn btn-primary mr-2">{{ __('label.simpan') }}</button>
                <button class="btn btn-danger" type="button" id="btnBack">{{ __('label.batal') }}</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('page_script')
<script type="text/javascript">
    var formValidator;

    $(document).ready(function() {

        appScript.combobox();
        appScript.autoNumeric();

        $('#tabel').DataTable({
            scrollCollapse: true,
            dom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            
            scrollX:true,
            ajax:{
                url : "{{ route('item.load_data') }}",
                type : 'GET',
                data : function (d) {
                    d.id_kategori = $('#fl_id_kategori').val();
                    d.id_satuan    = $('#fl_id_satuan').val();
                }
            },
            language: {
                url: datatableLang
            },
            columnDefs: [
                {visible: false, targets : []},
                {orderable: false, targets : ["column-checkbox",-1]},
                {
                    render: function ( data, type, row ) {
                       return '<label class="custom-control custom-checkbox">\
                                <input type="checkbox" class="custom-control-input select_all_child" id="cb-'+row.no+'" name="cb-'+row.no+'" value="'+data+'">\
                                <span class="custom-control-label">&nbsp;</span>\
                            </label>';
                    },
                    targets: ["column-checkbox"]
                },
                {
                    render: function ( data, type, row ) {
                        var edit = '<button type="button" title="{{ __('label.ubah') }}" data-toggle="tooltip" class="btn btn-primary ubah"><span class="{{ __('label.ubah_icon') }}"></span></button>';
                        return edit;
                    },
                    targets: [-1]
                },
            ],
            order: [[1,"asc"]],
            columns: [
                { width: "3%" , data: "id_target"},
                { width: "5%" , data: "no"},
                { width: "20%", data: "keterangan"},
                { width: "10%", data: "target"},
                { width: "6%",  data: "id_target" },
            ],
            drawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
                resetAksiLaindanCheckbox();
            },
        });

        $('#length_dropdown').on('click', '.length_option', function(event) {
            var value = $(this).data('value');
            var label = $(this).data('label');

            $('#length_dropdown').find('.current_length').text(label);

            var t = $('#tabel').DataTable();
            var length = value == 'All' ? -1 : value;
            t.page.len(length).draw();
        });

        formValidator = $('#form').validate({
            onkeyup: false,
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parents("div.form-group").find(".help-block"));
            },
            submitHandler: function(form) {
                saveData();
            }
        });

        $('#tabel tbody').on( 'click', '.ubah', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            showData(data.id_item);
        });

        $('#tabel tbody').on( 'click', '.hapus', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            appScript.swconfirm("{{ __('message.confirm') }}","{{ __('message.delete_confirm') }}",deleteData,data.id_item);
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.DataTable().search($(this).val()).draw();
        });

        $('#btnAdd').on('click', function(event) {
            openForm();
        });

        $('#btnBack').on('click', function(event) {
            closeForm();
        });

        $('#tabel tbody').on('click', 'input[type="checkbox"].select_all_child', function(event) {
            toggleSelectAllCheckbox();
        });

        $('#select_all').on('click', function(event) {
            $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').prop('checked', $(this).is(':checked'));
            toggleAksiLain();
        });

        $('#btnHapus').on('click', function(event) {
            var selectedUserGroupId = [];
            $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').each(function(index, el) {
                selectedUserGroupId.push($(this).val());
            });

            if (selectedUserGroupId.length > 0) {
                appScript.swconfirm("{{ __('message.confirm') }}","{{ __('message.delete_batch_confirm') }}",deleteBatchData,selectedUserGroupId);
            }
        });
    });

    function toggleSelectAllCheckbox() {
        var checkbox  = $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').length;
        var checkedCB = $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').length;
        $('#select_all').prop('checked', (checkbox == checkedCB));
        toggleAksiLain();
    }

    function toggleAksiLain() {
        var checkbox  = $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').length;
        var checkedCB = $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').length;
        $('#data_terpilih').text(checkedCB);
        $('#aksi_lain').find('button').prop('disabled',!(checkedCB > 0));
    }

    function resetAksiLaindanCheckbox() {
        $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').prop('checked', false);
        toggleSelectAllCheckbox();
        toggleAksiLain();
    }

    function reloadTable() {
        var t = $('#tabel').DataTable();
        t.ajax.reload();
    }

    function resetForm() {
        formValidator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('.cmb_select2').val('').trigger('change');
        $('input[name="_token"]').val('{{ csrf_token() }}');
    }

    function openForm() {
        resetForm();
        $('#tabel_card').hide();
        $('#form_card').show();
    }

    function closeForm() {
        $('#form_card').hide();
        $('#tabel_card').show();
    }

    function showData(id) {
        $.ajax({
            url: '{{ route('item.show') }}',
            type: 'GET',
            dataType: 'JSON',
            data: {
                id: id
            },
            success: function(response){
                var data = response.data;

                if (data != null) {
                    openForm();
                    $('#id_target').val(data.id_target);
                    $('#target').val(data.target);
                    $('#keterangan').val(data.keterangan).trigger('change');
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function saveData() {
        $.ajax({
            url: '{{ route('target.save') }}',
            type: 'POST',
            dataType: 'JSON',
            data: $('#form').serialize(),
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                if (response.success) {
                    closeForm();
                    reloadTable();
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){
        });
    }

    function deleteData(id) {
        $.ajax({
            url: '{{ route('target.delete') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                id: id,
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                reloadTable();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function deleteBatchData(batchId) {
        $.ajax({
            url: '{{ route('target.delete_batch') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                id: JSON.stringify(batchId)
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                reloadTable();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

</script>  

@endpush
