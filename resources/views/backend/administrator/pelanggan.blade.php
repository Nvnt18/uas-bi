@extends('layouts.base')

@include('plugin.highchart')
@include('plugin.datatable')
@include('plugin.datepicker')
@include('plugin.amchart')
@include('plugin.select2')

@section('content')

<div class="card">
    <div class="card-header row">
        <div class="col-md-7">
            <div class="input-group">
                <h3>Balanced Scorecard <b>Perspektif Pelanggan</b></h3>
            </div>
        </div>
        <div class="col col-md-2">
            <div class="input-group input-group-inverse">
                <button class="btn btn-outline-dark" type="button" onclick="reloadChart()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control tahun" value="{{ date('Y') }}" name="fl_date" data-target="#fl_date" id="fl_date" placeholder="Tahun">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 id="line_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="line_chart"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 id="on_time_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="on_time_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header row">
        <div class="col col-md-12">
            <div class='buttons'>
            @foreach ($year as $key => $value)
                @if($key != 0 && $key == (sizeOf($year)-1))
                <button id='{{ $value }}' class='active'>
                    {{ $value }}
                </button>
                @elseif($key != 0 && $key)
                <button id='{{ $value }}'>
                    {{ $value }}
                </button>
                @endif
            @endforeach
            </div>
        </div>
    </div>
    <div class="card-body">
        <figure class="highcharts-figure">
            <div id="comparison_chart"></div>
        </figure>
    </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
    
    $(document).ready(function() {
        appScript.yearPicker();	

        loadLineChart();
        loadOnTimeChart();
        loadComparisonChart();
        getYear($('#fl_date').val());

        $('#fl_jumlah').on('change', function(event) {
            reloadChart();
        }); 

        $('#fl_date').on('change.datetimepicker', function(event) {
            reloadChart();

            getYear($('#fl_date').val());
        });
    });

    function reloadChart(){
        loadLineChart();
        loadOnTimeChart();
    }

    function getYear(date){
        var tahun = (date ? moment(date,'YYYY').format('YYYY') : '');

        $('#on_time_text').text('Ketepatan Pengiriman Pesanan Tahun '+tahun);
        $('#line_text').text('Jumlah Pelanggan 5 Tahun Terakhir');
    }

    function loadLineChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_orders') }}",
            type : 'GET',
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    console.log(data);
                    lineChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadComparisonChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_comparison') }}",
            type : 'GET',
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    comparisonChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadOnTimeChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_on_time') }}",
            type : 'GET',
            data: {
                date: $('#fl_date').val()
            },
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    onTimeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function getData(data) {
        return data.map(function (country, i) {
            return {
                name: country[0],
                y: country[1],
                color: 'rgb(0, 82, 180)'
            };
        });
    }

    function comparisonChart(data){
        var chart = Highcharts.chart('comparison_chart', {
            chart: {
                type: 'column'
            },
            title:false,
            plotOptions: {
                series: {
                    grouping: false,
                    borderWidth: 0
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                shared: true,
                headerFormat: '<span style="font-size: 15px">{point.point.name}</span><br/>',
                pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y} orders</b><br/>'
            },
            xAxis: {
                type: 'category',
                max: 4,
                labels: {
                    useHTML: true,
                    animate: true,
                    formatter: function () {
                        var value = this.value,
                            output;

                        data.dataCountry.forEach(function (country) {
                            if (country.name === value) {
                                output = country.name;
                            }
                        });

                        return output;
                    }
                }
            },
            yAxis: [{
                title: {
                    text: 'Orders'
                },
                showFirstLabel: false
            }],
            series: [{
                color: 'rgb(158, 159, 163)',
                pointPlacement: -0.2,
                linkedTo: 'main',
                data: data.dataPrev[data.yearNow].slice(),
                name: data.yearNow-1
            }, {
                name: data.yearNow,
                id: 'main',
                dataSorting: {
                    enabled: true,
                    matchByName: true
                },
                dataLabels: [{
                    enabled: true,
                    inside: true,
                    style: {
                        fontSize: '16px'
                    }
                }],
                data: getData(data.dataNow[data.yearNow]).slice()
            }],
            exporting: {
                allowHTML: true
            }
        });

        var years = data.year;

        years.forEach(function (year) {
            var btn = document.getElementById(year);

            btn.addEventListener('click', function () {

                document.querySelectorAll('.buttons button.active').forEach(function (active) {
                    active.className = '';
                });
                btn.className = 'active';

                chart.update({
                    title: {
                        text: 'Daftar 5 Negara dengan Pemesanan Terbanyak Tahun ' + year
                    },
                    subtitle: {
                        text: 'Data orders dibandingkan dari data Tahun ' + (year - 1)
                    },
                    series: [{
                        name: year - 1,
                        data: data.dataPrev[year].slice()
                    }, {
                        name: year,
                        data: getData(data.dataNow[year]).slice()
                    }]
                }, true, false, {
                    duration: 800
                });
            });
        });
    }

    function lineChart(data) {
        Highcharts.chart('line_chart', {
            title: false,
            yAxis: {
                title: {
                    text: 'Jumlah Pelanggan'
                }
            },
            xAxis: {
                categories: data.categories
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    }
                }
            },
            series: [{
                name: 'Pelanggan',
                data: data.data
            },{
                name: 'Target',
                data: [50,50,50],
                color: '#fb6340'
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    }

    function onTimeChart(data){
        Highcharts.chart('on_time_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: data.status
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} Orders)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Ketepatan',
                colorByPoint: true,
                data: [{
                    name: 'Tepat Waktu',
                    y: data.on_time,
                    color: '#2dce89'
                }, {
                    name: 'Terlambat',
                    y: data.late,
                    color: '#f5365c'
                }]
            }]
        });
    }
</script>
@endpush