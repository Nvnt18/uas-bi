@extends('layouts.base')

@include('plugin.highchart')
@include('plugin.datatable')
@include('plugin.datepicker')
@include('plugin.amchart')

@section('content')

<div class="card home_card">
    <div class="card-header row">   
        <div class="col-md-7">
            <div class="input-group">
                <h3>Balanced Scorecard <b>Perspektif Tumbuh dan Berkembang</b></h3>
            </div>
        </div>
        <div class="col col-md-2">
            <div class="input-group input-group-inverse">
                <button class="btn btn-outline-dark" type="button" onclick="reloadTable()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control bulan_tahun" value="{{ date('m-Y') }}" name="fl_date" data-target="#fl_date" id="fl_date" placeholder="Bulan">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 id="sales_text" style="text-align: center; width: 100%;"></h3>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="sales_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="card" id="tabel_card">
        <form class="forms-sample" id="form" action="javascript:;">
            <div class="card-header row">
                <div class="col col-md-7">
                    <div class="card-search with-adv-search dropdown">
                        <form action="javascript:;">
                            <input type="text" id="input_pencarian" autofocus class="form-control" placeholder="{{ __('label.cari_hint') }}" required="">
                            <button type="button" class="btn btn-icon"><i class="{{ __('label.cari_icon') }}"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col col-md-1">
                    <button class="btn btn-outline-dark" type="button" onclick="reloadTable()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
                </div>
                <div class="col col-md-4">
                    <div class="card-options text-right" id="length_dropdown">
                        <span class="text-muted text-small mr-2">Tampilkan: </span>
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="current_length">Semua</span><i class="ik ik-chevron-down mr-0 align-middle"></i>
                        </button>
                        data
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1617px, 30px, 0px);">
                            <a class="dropdown-item length_option" data-value="All" data-label="Semua" href="javascript:;">Semua</a>
                            <a class="dropdown-item length_option" data-value="10" data-label="10" href="javascript:;">10</a>
                            <a class="dropdown-item length_option" data-value="20" data-label="25" href="javascript:;">25</a>
                            <a class="dropdown-item length_option" data-value="50" data-label="50" href="javascript:;">50</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="dt-responsive">
                    <table id="tabel" class="table table-inverse table-hover borderless" width="100%">
                        <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Nama Lengkap</th>
                                <th>Jabatan </th>
                                <th>Cabang</th>
                                <th>Jumlah Pelanggan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
    
    $(document).ready(function() {
        appScript.monthYearPicker();	

        getMonth($('#fl_date').val());

        loadSalesChart();

        $('#tabel').DataTable({
            scrollCollapse: true,
            dom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            iDisplayLength: -1,
            scrollX:true,
            ajax:{
                url : "{{ route('growth.load_data') }}",
                type : 'GET',
                data : function(d){
                    d.date = $('#fl_date').val();
                },
            },
            language: {
                url: datatableLang
            },
            columnDefs: [
                {visible: false, targets : []},
                {orderable: false, targets : ["column-checkbox",-1]},
                {
                    render: function ( data, type, row ) {
                    	if (type == 'sort') {
                    		return data;
                    	}
                    	return data >= 1 ? '<span class="badge badge-success">Terpenuhi</span>' : '<span class="badge badge-danger">Tidak Terpenuhi</span>';
                    },
                    targets: [5]
                },
            ],
            order: [[4,"desc"]],
            columns: [
                { width: "15%", data: "id"},
                { width: "15%", data: "nama"},
                { width: "15%", data: "jabatan"},
                { width: "15%", data: "cabang"},
                { width: "15%", data: "jumlah_pelanggan"},
                { width: "15%", data: "jumlah_pelanggan"}
            ],
            drawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#length_dropdown').on('click', '.length_option', function(event) {
            var value = $(this).data('value');
            var label = $(this).data('label');

            $('#length_dropdown').find('.current_length').text(label);

            var t = $('#tabel').DataTable();
            var length = value == 'All' ? -1 : value;
            t.page.len(length).draw();
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.DataTable().search($(this).val()).draw();
        });

        $('#fl_date').on('change.datetimepicker', function(event) {
            reloadTable();
            
            getMonth($('#fl_date').val());
        });
    });

    function getMonth(date){
        var bulan = (date ? moment(date,'MM-YYYY').format('MMMM') : '');

        $('#sales_text').text('Persentase Karyawan yang memenuhi Penjualan tiap bulan (Bulan '+bulan+')');
    }

    function reloadTable() {
        var t = $('#tabel').DataTable();
        t.ajax.reload();
        loadSalesChart();
    }

    function loadSalesChart(){
        $.ajax({
            url : "{{ route('growth.load_data_sales') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    salesChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function salesChart(data){
        Highcharts.chart('sales_chart', {
            chart: {
                type: 'pie'
            },
            title: false,
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}: {point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total<br/>'
            },

            series: [
                {
                    name: "Sales",
                    colorByPoint: true,
                    data: data.good_or_bad_data
                }
            ],
            drilldown: {
                series: data.drilldown
            }
        });
    }

</script>
@endpush