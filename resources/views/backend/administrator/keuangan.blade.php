@extends('layouts.base')

@include('plugin.highchart')
@include('plugin.datatable')
@include('plugin.datepicker')
@include('plugin.amchart')

@section('content')

<div class="card home_card">
    <div class="card-header row">
        <div class="col-md-7">
            <div class="input-group">
                <h3>Balanced Scorecard <b>Perspektif Keuangan</b></h3>
            </div>
        </div>
        <div class="col col-md-2">
            <div class="input-group input-group-inverse">
                <button class="btn btn-outline-dark" type="button" onclick="reloadChart()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control bulan_tahun" value="{{ date('m-Y') }}" name="fl_date" data-target="#fl_date" id="fl_date" placeholder="Bulan">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="income_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="income_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="net_profit_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="net_profit_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="order_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="order_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script type="text/javascript">
    
    $(document).ready(function() {
        appScript.monthYearPicker();	

        getMonth($('#fl_date').val());
        loadIncomeChart();
        loadNetProfitChart();
        loadOrderChart();

        $('#fl_date').on('change.datetimepicker', function(event) {
            reloadChart();
            
            getMonth($('#fl_date').val());
        });
    });

    function getMonth(date){
        var bulan = (date ? moment(date,'MM-YYYY').format('MMMM') : '');

        if(bulan != 'Januari'){
            $('#order_text').text('Status Kredit Januari - '+bulan);
            $('#income_text').text('Income bulan '+bulan);
            $('#net_profit_text').text('Net Profit bulan '+bulan);
        }else{
            $('#order_text').text('Status Kredit bulan Januari');
            $('#income_text').text('Income bulan '+bulan);
            $('#net_profit_text').text('Net Profit bulan '+bulan);
        }
    }

    function reloadChart(){
        loadIncomeChart();
        loadNetProfitChart();
        loadOrderChart();
    }

    function loadIncomeChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_income') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    incomeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadNetProfitChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_net_profit') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    netProfitChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadOrderChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_orders') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    Highcharts.chart('order_chart', {
                        chart: {
                            height: 210,
                            marginBottom: -20,
                            marginTop: -20
                        },
                        title: {
                            useHTML: true,
                            text: data.badge,
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 6
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>'+numberWithCommas("{point.y}")+' ({point.percentage:.1f}%)</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: true,
                                    distance: -25,
                                    style: {
                                        fontWeight: 'bold',
                                        color: 'white',
                                        fontSize: '10px'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '110%'
                            }
                        },
                        exporting:false,
                        series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '50%',
                            data: [
                                {
                                    name: 'Sudah dibayar',
                                    y: data.lunas,
                                    color: '#2dce89'
                                },
                                {
                                    name: 'Belum dibayar',
                                    y: data.belum,
                                    color: '#f5365c'
                                }
                            ]
                        }]
                    });
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function netProfitChart(data){
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'solid'
                }
            },
            tooltip: {
                enabled: false
            },
            yAxis: {
                plotBands: [{
                    from: 1,
                    to: (0.3 * data.target_net_profit),
                    color: '#f5365c',
                    thickness: '40%'
                }, {
                    from: (0.3 * data.target_net_profit)+1,
                    to: (0.7 * data.target_net_profit),
                    color: '#fb6340',
                    thickness: '40%'
                }, {
                    from: (0.7 * data.target_net_profit),
                    to: data.target_net_profit,
                    color: '#2dce89',
                    thickness: '40%'
                }],
                lineWidth: 0,
                minorTickInterval: 1,
                tickPositions:[1,data.target_net_profit],
                tickAmount: 1,
                min: 1,
                max: data.target_net_profit,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                    },
                            marker: {
                    enabled: true,
                    symbol: 'triangle',
                    }
                },
            }
        };

        var chartNetProfit = Highcharts.chart('net_profit_chart', Highcharts.merge(gaugeOptions, {
            yAxis: {
                title: {
                    text: 'Net Profit'
                },
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Net Profit',
                tooltip: {
                    valueSuffix: ' USD'
                },
            },
            {
                name: 'Net Profit Dot',
                type: 'gauge',
                data: [data.point],
                dial: {
                    rearLength: '-121%'
                },
                dataLabels: {
                    useHTML: true,
                    format: data.badge + '' + numberWithCommas(data.text) + ' USD</span>'
                },
                pivot: {
                    radius: 0
                }
            }]

        }));
    }

    function incomeChart(data){
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'solid'
                }
            },
            tooltip: {
                enabled: false
            },
            yAxis: {
                plotBands: [{
                    from: 1,
                    to: (0.3 * data.target_income),
                    color: '#f5365c',
                    thickness: '40%'
                }, {
                    from: (0.3 * data.target_income)+1,
                    to: (0.7 * data.target_income),
                    color: '#fb6340',
                    thickness: '40%'
                }, {
                    from: (0.7 * data.target_income),
                    to: data.target_income,
                    color: '#2dce89',
                    thickness: '40%'
                }],
                lineWidth: 0,
                minorTickInterval: 1,
                tickPositions:[1,data.target_income],
                tickAmount: 1,
                min: 1,
                max: data.target_income,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                    },
                            marker: {
                    enabled: true,
                    symbol: 'triangle',
                    }
                },
            }
        };

        var chartIncome = Highcharts.chart('income_chart', Highcharts.merge(gaugeOptions, {
            yAxis: {
                title: {
                    text: 'Income'
                },
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Income',
                tooltip: {
                    valueSuffix: ' USD'
                },
            },
            {
                name: 'Income Dot',
                type: 'gauge',
                data: [data.point],
                dial: {
                    rearLength: '-121%'
                },
                dataLabels: {
                    useHTML: true,
                    format: data.badge + '' + numberWithCommas(data.text) + ' USD</span>'
                },
                pivot: {
                    radius: 0
                }
            }]

        }));
    }
    
    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>
@endpush