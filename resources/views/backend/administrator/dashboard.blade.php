@extends('layouts.base')

@include('plugin.datatable')
@include('plugin.highchart')
@include('plugin.datepicker')
@include('plugin.amchart')

@section('content')

<div class="card home_card">
    <div class="card-header row">
        <div class="col-md-7">
            <div class="input-group">
                <h3>Selamat Datang di Website <b>UAS Business Intelligence</b></h3>
            </div>
        </div>
        <div class="col col-md-2">
            <div class="input-group input-group-inverse">
                <button class="btn btn-outline-dark" type="button" onclick="reloadChart()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
            </div>
        </div>
        <div class="col-md-3">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control bulan_tahun" value="{{ date('m-Y') }}" name="fl_date" data-target="#fl_date" id="fl_date" placeholder="Bulan">
            </div>
        </div>
        <div class="col-md-2" style="display:none">
            <div class="input-group input-group-inverse">
                <span class="input-group-prepend"><label class="input-group-text"><i class="fa fa-calendar"></i></label></span>
                <input type="text" class="form-control tahun" value="{{ date('Y') }}" name="fl_date2" data-target="#fl_date2" id="fl_date2" placeholder="Tahun">
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="income_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="income_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="net_profit_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="net_profit_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="order_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="order_chart" class="chart-container"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="sales_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="sales_chart"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card" id="tabel_card">
        <form class="forms-sample" id="form" action="javascript:;">
            <div class="card-header row">
                <div class="col col-md-12">
                    <div class="card-search with-adv-search dropdown">
                        <form action="javascript:;">
                            <input type="text" id="input_pencarian" autofocus class="form-control" placeholder="{{ __('label.cari_hint') }}" required="">
                            <button type="button" class="btn btn-icon"><i class="{{ __('label.cari_icon') }}"></i></button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="dt-responsive">
                    <table id="tabel" class="table table-inverse table-hover borderless" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama Lengkap</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="line_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="line_chart"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="on_time_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="on_time_chart"></div>
                </figure>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h6 id="status_text" style="text-align: center; width: 100%;"></h6>
            </div>
            <div class="card-body">
                <figure class="highcharts-figure">
                    <div id="status_chart"></div>
                </figure>
            </div>
        </div>
    </div>
</div>

<!-- @if($status == 1)
<div class="card">
    <div class="card-header">
        <h3 class="font-weight-bold">Daftar Kelompok</h3>
    </div>
</div>
@endif -->


<!-- @if($status == 0)
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header"><h3 class="font-weight-bold">Daftar Kelompok</h3></div>
            <div class="card-body">
                <div class="dt-responsive mt-0">
                    <h6>Tidak ada anggota</h6> 
                </div>  
            </div>
        </div>
    </div>
</div>
@else
    <div class="row">
    @foreach ($users as $key => $user)
    
    @if($key % 3 == 0)
    </div>
    <div class="row">
        <div class="col-md-4 text-center">
            <div class="card">
                <div class="card-header"><h3 class="font-weight-bold">Anggota {{ $user['no'] }}</h3></div>
                <div class="card-body">
                    <div class="dt-responsive mt-0">
                    <img src="{{ $user['avatar'] ? asset('assets/uploads/avatar/'.$user['avatar']) : asset('assets/img/avatar.png') }}" class="rounded" height="250" onerror="this.src='{{ asset('assets/img/not-available.jpg') }}'">
                    </div>  
                </div>
                <div class="card-footer">
                    <h6 class="font-weight-bold">{{ $user['name'] }}</h6>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-4 text-center">
            <div class="card">
                <div class="card-header"><h3 class="font-weight-bold">Anggota {{ $user['no'] }}</h3></div>
                <div class="card-body">
                    <div class="dt-responsive mt-0">
                    <img src="{{ $user['avatar'] ? asset('assets/uploads/avatar/'.$user['avatar']) : asset('assets/img/avatar.png') }}" class="rounded" width="250" onerror="this.src='{{ asset('assets/img/not-available.jpg') }}'">
                    </div>  
                </div>
                <div class="card-footer">
                    <h6 class="font-weight-bold">{{ $user['name'] }}</h6>
                </div>
            </div>
        </div>
    @endif
    
    @endforeach
    </div>
@endif -->
@endsection

@push('script')
<script type="text/javascript">
    $(document).ready(function() {
        appScript.monthYearPicker();	
        appScript.yearPicker();	

        getMonth($('#fl_date').val());
        loadIncomeChart();
        loadNetProfitChart();
        loadOrderChart();
        loadSalesChart();
        loadLineChart();
        loadOnTimeChart();
        loadStatusChart();

        $('#tabel').DataTable({
            scrollCollapse: true,
            dom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            iDisplayLength: 5,
            scrollX:true,
            ajax:{
                url : "{{ route('growth.load_data') }}",
                type : 'GET',
                data : function(d){
                    d.date = $('#fl_date').val();
                },
            },
            language: {
                url: datatableLang
            },
            columnDefs: [
                {visible: false, targets : []},
                {orderable: false, targets : ["column-checkbox",-1]},
                {
                    render: function ( data, type, row ) {
                    	if (type == 'sort') {
                    		return data;
                    	}
                    	return data >= 1 ? '<span class="badge badge-success">Terpenuhi</span>' : '<span class="badge badge-danger">Tidak Terpenuhi</span>';
                    },
                    targets: [4]
                },
            ],
            order: [[3,"desc"]],
            columns: [
                { width: "5%", data: "no"},
                { width: "10%", data: "id"},
                { width: "15%", data: "nama"},
                { width: "15%", data: "jumlah_pelanggan"},
                { width: "10%", data: "jumlah_pelanggan"}
            ],
            drawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
        });

        $('#length_dropdown').on('click', '.length_option', function(event) {
            var value = $(this).data('value');
            var label = $(this).data('label');

            $('#length_dropdown').find('.current_length').text(label);

            var t = $('#tabel').DataTable();
            var length = value == 'All' ? -1 : value;
            t.page.len(length).draw();
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.DataTable().search($(this).val()).draw();
        });

        $('#fl_date').on('change.datetimepicker', function(event) {
            var tahun = ($('#fl_date').val() ? moment($('#fl_date').val(),'MM-YYYY').format('YYYY') : '');
            $('#fl_date2').val(tahun);
            reloadChart();
            
            getMonth($('#fl_date').val());
        });
    });

    function getMonth(date){
        var bulan = (date ? moment(date,'MM-YYYY').format('MMMM') : '');
        var tahun = (date ? moment(date,'MM-YYYY').format('YYYY') : '');

        $('#sales_text').text('% Penjualan Karyawan Bulan '+bulan);
        $('#on_time_text').text('Ketepatan Pengiriman Tahun '+tahun);
        $('#line_text').text('Jumlah Pelanggan 5 Tahun Terakhir');
        $('#status_text').text('Status Pesanan Tahun '+tahun);

        if(bulan != 'Januari'){
            $('#order_text').text('Status Kredit Januari - '+bulan);
            $('#income_text').text('Income bulan '+bulan);
            $('#net_profit_text').text('Net Profit bulan '+bulan);
        }else{
            $('#order_text').text('Status Kredit bulan Januari');
            $('#income_text').text('Income bulan '+bulan);
            $('#net_profit_text').text('Net Profit bulan '+bulan);
        }
    }

    function reloadChart(){
        var t = $('#tabel').DataTable();
        t.ajax.reload();

        loadIncomeChart();
        loadNetProfitChart();
        loadOrderChart();
        loadSalesChart();
        loadLineChart();
        loadOnTimeChart();
        loadOnTimeChart();
        loadStatusChart();
    }

    function loadStatusChart(){
        $.ajax({
            url : "{{ route('internal.load_data_status') }}",
            type : 'GET',
            data: {
                date: $('#fl_date2').val()
            },
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    statusChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadSalesChart(){
        $.ajax({
            url : "{{ route('growth.load_data_sales') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    salesChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadIncomeChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_income') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    incomeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadNetProfitChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_net_profit') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    netProfitChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadOrderChart(){
        $.ajax({
            url : "{{ route('keuangan.load_data_orders') }}",
            type : 'GET',
            data: { 
                date: $('#fl_date').val() 
            },
            success: function(response){
                var data = response;
                if (data != null) {
                    Highcharts.chart('order_chart', {
                        chart: {
                            height: 210,
                            marginBottom: -20,
                            marginTop: -20
                        },
                        title: {
                            useHTML: true,
                            text: data.badge,
                            align: 'center',
                            verticalAlign: 'middle',
                            y: 63
                        },
                        tooltip: {
                            pointFormat: '{series.name}: <b>'+numberWithCommas("{point.y}")+' ({point.percentage:.1f}%)</b>'
                        },
                        accessibility: {
                            point: {
                                valueSuffix: '%'
                            }
                        },
                        plotOptions: {
                            pie: {
                                dataLabels: {
                                    enabled: true,
                                    distance: -25,
                                    style: {
                                        fontWeight: 'bold',
                                        color: 'white',
                                        fontSize: '10px'
                                    }
                                },
                                startAngle: -90,
                                endAngle: 90,
                                center: ['50%', '75%'],
                                size: '110%'
                            }
                        },
                        exporting:false,
                        series: [{
                            type: 'pie',
                            name: 'Browser share',
                            innerSize: '50%',
                            data: [
                                {
                                    name: 'Sudah dibayar',
                                    y: data.lunas,
                                    color: '#2dce89'
                                },
                                {
                                    name: 'Belum dibayar',
                                    y: data.belum,
                                    color: '#f5365c'
                                }
                            ]
                        }]
                    });
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadLineChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_orders') }}",
            type : 'GET',
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    lineChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loadOnTimeChart(){
        $.ajax({
            url : "{{ route('pelanggan.load_data_on_time') }}",
            type : 'GET',
            data: {
                date: $('#fl_date2').val()
            },
            dataType: 'JSON',
            success: function(response){
                var data = response;
                if (data != null) {
                    onTimeChart(data);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function statusChart(data){
        Highcharts.chart('status_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: data.status
            },
            exporting:false,
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} Orders)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Status',
                colorByPoint: true,
                data: data.data
            }]
        });
    }

    function lineChart(data) {
        Highcharts.chart('line_chart', {
            title: false,
            exporting:false,
            yAxis: {
                title: {
                    text: 'Jumlah Pelanggan'
                }
            },
            xAxis: {
                categories: data.categories
            },
            legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
            },
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    }
                }
            },
            series: [{
                name: 'Pelanggan',
                data: data.data
            },{
                name: 'Target',
                data: [50,50,50],
                color: '#fb6340'
            }],
            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        legend: {
                            layout: 'horizontal',
                            align: 'center',
                            verticalAlign: 'bottom'
                        }
                    }
                }]
            }
        });
    }

    function onTimeChart(data){
        Highcharts.chart('on_time_chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                useHTML: true,
                text: data.status
            },
            exporting:false,
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}% ({point.y} Orders)</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Ketepatan',
                colorByPoint: true,
                data: [{
                    name: 'Tepat Waktu',
                    y: data.on_time,
                    color: '#2dce89'
                }, {
                    name: 'Terlambat',
                    y: data.late,
                    color: '#f5365c'
                }]
            }]
        });
    }

    function salesChart(data){
        Highcharts.chart('sales_chart', {
            chart: {
                type: 'pie',
                height: 300
            },
            title: {
                useHTML: true,
                text: data.title
            },
            exporting:false,
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                series: {
                    dataLabels: {
                        enabled: false,
                        format: '{point.name}: {point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b> of total ({point.percentage:.1f}%)<br/>'
            },

            series: [
                {
                    name: "Sales",
                    colorByPoint: true,
                    data: data.good_or_bad_data
                }
            ],
            drilldown: {
                series: data.drilldown
            }
        });
    }

    function netProfitChart(data){
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            exporting:false,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'solid'
                }
            },
            tooltip: {
                enabled: false
            },
            yAxis: {
                plotBands: [{
                    from: 1,
                    to: (0.3 * data.target_net_profit),
                    color: '#f5365c',
                    thickness: '40%'
                }, {
                    from: (0.3 * data.target_net_profit)+1,
                    to: (0.7 * data.target_net_profit),
                    color: '#fb6340',
                    thickness: '40%'
                }, {
                    from: (0.7 * data.target_net_profit),
                    to: data.target_net_profit,
                    color: '#2dce89',
                    thickness: '40%'
                }],
                lineWidth: 0,
                minorTickInterval: 1,
                tickPositions:[1,data.target_net_profit],
                tickAmount: 1,
                min: 1,
                max: data.target_net_profit,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                    },
                            marker: {
                    enabled: true,
                    symbol: 'triangle',
                    }
                },
            }
        };

        var chartNetProfit = Highcharts.chart('net_profit_chart', Highcharts.merge(gaugeOptions, {
            yAxis: {
                title: {
                    useHTML: true,
                    text: data.title,
                    y: -80
                },
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Net Profit',
                tooltip: {
                    valueSuffix: ' USD'
                },
            },
            {
                name: 'Net Profit Dot',
                type: 'gauge',
                data: [data.point],
                dial: {
                    rearLength: '-121%'
                },
                dataLabels: {
                    useHTML: true,
                    format: data.badge + '' + numberWithCommas(data.text) + ' USD</span>'
                },
                pivot: {
                    radius: 0
                }
            }]

        }));
    }

    function incomeChart(data){
        var gaugeOptions = {
            chart: {
                type: 'solidgauge'
            },
            title: null,
            exporting:false,
            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'solid'
                }
            },
            tooltip: {
                enabled: false
            },
            yAxis: {
                plotBands: [{
                    from: 0,
                    to: (0.3 * data.target_income),
                    color: '#f5365c',
                    thickness: '40%'
                }, {
                    from: (0.3 * data.target_income)+1,
                    to: (0.7 * data.target_income),
                    color: '#fb6340',
                    thickness: '40%'
                }, {
                    from: (0.7 * data.target_income),
                    to: data.target_income,
                    color: '#2dce89',
                    thickness: '40%'
                }],
                lineWidth: 0,
                minorTickInterval: 1,
                tickPositions:[1,data.target_income],
                tickAmount: 1,
                min: 1,
                max: data.target_income,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                    },
                            marker: {
                    enabled: true,
                    symbol: 'triangle',
                    }
                },
            }
        };

        var chartIncome = Highcharts.chart('income_chart', Highcharts.merge(gaugeOptions, {
            yAxis: {
                title: {
                    useHTML: true,
                    text: data.title,
                    y: -80
                },
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Income',
                tooltip: {
                    valueSuffix: ' USD'
                },
            },
            {
                name: 'Income Dot',
                type: 'gauge',
                data: [data.point],
                dial: {
                    rearLength: '-121%'
                },
                dataLabels: {
                    useHTML: true,
                    format: data.badge + '' + numberWithCommas(data.text) + ' USD</span>'
                },
                pivot: {
                    radius: 0
                }
            }]

        }));
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>
@endpush