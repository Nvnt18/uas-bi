@extends('layouts.base')

@include('plugin.jquery-validation')

@section('content')

<div class="row">
	<div class="col-lg-4 col-md-5">
		<div class="card">
			<div class="card-body">
				<div class="text-center"> 
					<img src="{{ $profile->avatar ? asset('assets/uploads/avatar/'.$profile->avatar) : asset('assets/img/avatar.png') }}" class="rounded" width="150" onerror="this.src='{{ asset('assets/img/not-available.jpg') }}'">
					<h4 class="card-title mt-10">{{ $profile->name }}</h4>
					<p class="card-subtitle">{{ $profile->userGroup->user_group_name }}</p>
				</div>
			</div>
			<hr class="mb-0"> 
			<div class="card-body"> 
				<small class="text-muted d-block">Email </small>
				<h6>{{ $profile->email }}</h6>
				<small class="text-muted d-block">User Group </small>
				<h6>{{ $profile->userGroup->user_group_name }}</h6>  
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-md-7">
		<form id="form" class="form-horizontal" action="javascript:;" method="POST" enctype="multipart/form-data">
		<div class="card">
            <div class="card-header"><h6 class="font-weight-bold">Setting Profile</h6></div>
			<div class="card-body">
					@csrf
					<input type="hidden" name="id" value="{{ $profile->id }}">
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" name="email" id="email" readonly value="{{ $profile->email }}">
	                    <span class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="user_group">User Group</label>
						<input type="user_group" class="form-control" name="user_group" id="user_group" readonly value="{{ $profile->userGroup->user_group_name }}">
	                    <span class="help-block"></span>
					</div>
					<div class="form-group">
						<label for="name">Nama <span class="text-red">*</span></label>
						<input type="text" class="form-control" name="name" id="name" value="{{ $profile->name }}">
	                    <span class="help-block"></span>
					</div>
					<div class="form-group" id="checkbox_ubah_password">
	                    <label class="custom-control custom-checkbox">
	                        <input type="checkbox" class="custom-control-input" name="ubah_password" id="ubah_password" onclick="toggleUbahPassword($(this))">
	                        <span class="custom-control-label">&nbsp;Centang untuk mengubah password</span>
	                    </label>
	                </div>
	                <div class="form-group password_fill"  style="display: none">
	                    <label for="password">Password <span class="text-red">*</span></label>
	                    <div class="input-group">
	                        <input type="password" id="password" name="password" class="form-control" required>
	                        <span class="input-group-append">
	                            <label class="input-group-text" title="Show/Hide Password" data-toggle="tooltip" onclick="toggleInputPassword($(this))" style="cursor: pointer;"><span class="fa fa-eye-slash"></span></label>
	                        </span>
	                    </div>
	                    <span class="help-block"></span>
	                </div>
	                <div class="form-group password_fill"  style="display: none">
	                    <label for="password_confirm">Konfirmasi Password <span class="text-red">*</span></label>
	                    <div class="input-group">
	                        <input type="password" id="password_confirm" name="password_confirm" class="form-control" required>
	                        <span class="input-group-append">
	                            <label class="input-group-text" title="Show/Hide Password" data-toggle="tooltip" onclick="toggleInputPassword($(this))" style="cursor: pointer;"><span class="fa fa-eye-slash"></span></label>
	                        </span>
	                    </div>
	                    <span class="help-block"></span>
	                </div>
	                <div class="row">
	                	<div class="col-md-2" style="text-align: center !important">
	                		<img src="{{ $profile->avatar ? asset('assets/uploads/avatar/'.$profile->avatar) : asset('assets/img/avatar.png') }}" alt="Avatar" id="avatar_preview" class="img-fluid rounded img-thumbnail" width="60" onerror="this.src='{{ asset('assets/img/not-available.jpg') }}'">
	                	</div>
	                	<div class="col-md-10">
	                		<div class="form-group">
	                			<label for="avatar">Avatar</label>
	                			<input type="file" data-default_value="{{ $profile->avatar ? asset('assets/uploads/avatar/'.$profile->avatar) : asset('assets/img/avatar.png') }}" id="avatar" name="avatar" class="file-upload-default" accept=".png, .jpg, .jpeg">
	                			<div class="input-group col-xs-12">
	                				<input type="text" class="form-control file-upload-info" id="file_name_avatar" disabled="" placeholder="File Foto (.png, .jpg, .jpeg) Maks. 1 MB">
	                				<span class="input-group-append">
	                					<button class="file-upload-browse btn btn-primary" type="button" id="btn_pilih_avatar" onclick="appScript.triggerFileUploadCustom($(this))" title="Pilih File" data-toggle="tooltip"><span class="fa fa-file-upload"></span></button>
	                					<button class="file-upload-browse btn btn-danger" style="display: none" id="btn_batal_avatar" type="button" onclick="batalUbahAvatar($(this))" title="Hapus File" data-toggle="tooltip"><span class="fa fa-times"></span></button>
	                				</span>
	                			</div>
	                			<span class="help-block">@error('avatar') {{ $message }} @enderror</span>
	                		</div>
	                	</div>
	                </div>
                    <div class="form-group">
                        <label class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" name="reset_avatar" id="reset_avatar" onclick="toggleDefaultAvatar($(this))">
                            <span class="custom-control-label">&nbsp;Centang untuk mengembalikan avatar ke pengaturan bawaan</span>
                        </label>
                    </div>
	                <div id="error_msg" class="mt-10" style="display: none">
	                	
	                </div>
			</div>
			<div class="card-footer">
				<button class="btn btn-primary" type="submit">Update Profile</button>
			</div>
		</div>
		</form>
	</div>
</div>

@endsection

@push('page_script')
<script type="text/javascript">
    var formValidator;

    $(document).ready(function() {

    	appScript.fileUploadCustom();

    	formValidator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parents("div.form-group").find(".help-block"));
            },
            rules:{
                password:{
                    required: true,
                    minlength: 6
                },
                password_confirm:{
                    required: true,
                    equalTo: "#password"
                },
            },
            submitHandler: function(form) {
                saveData();
            }
        });

        $("#avatar").on('change', function(event) {

            var reader = new FileReader();
            reader.onload = function(){
                var output = document.getElementById('avatar_preview');
                output.src = reader.result;
            };

        	if (event.target.files[0]) {
	            reader.readAsDataURL(event.target.files[0]);
        	} else{
        		var output = document.getElementById('avatar_preview');
                output.src = $(this).data('default_value');
        	}

            if ($(this).val()) {
            	$('#btn_batal_avatar').show();
            } else{
            	$('#btn_batal_avatar').hide();
            }
        });
    });

    function saveData(){
        var formData = new FormData($("#form")[0]);
    	$.ajax({
            url: '{{ route('profile.save') }}',
            type: 'POST',
            dataType: 'JSON',
            data: formData,
            contentType: false,
            processData: false,
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                let judulNotifikasi = response.success ? 'Sukses' : 'Gagal';
                appScript.swalert(judulNotifikasi,response.message,jenisNotifikasi,function(){
                	if (response.success) {
                		window.location.reload();
                	}
                });
            },
            error: function( jqXHR, textStatus, errorThrown ){
            	var errors = jqXHR.responseJSON.errors;
            	var errorMessage = [];
            	Object.keys(errors).forEach(function (key, index) {
            		$.each(errors[key], function(index, val) {
            			errorMessage.push(errors[key][index]);
            		});
            	});

            	errorMessage = errorMessage.map(function(elem, index) {
            		return '<li>'+elem+'</li>';
            	})

            	var htmlError = '<ul>'+errorMessage.join('')+'</ul>';

            	$('#error_msg').html('<div class="alert alert-danger" role="alert">\
            		'+htmlError+'\
            	</div>').show();

                setTimeout(function(){
                    $("#error_msg").empty().fadeOut('slow');
                }, 5000);

                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){

        });
    }

    function batalUbahAvatar(element){
        var inputFile = element.parents('div.form-group').find('input[type=file]').val(null).trigger('change');
    }

    function toggleUbahPassword(element) {
        if (element.is(':checked')) {
            $('.password_fill').show();
        } else{
            $('.password_fill').hide();
        }
    }

    function toggleDefaultAvatar(element) {
        if (element.is(':checked')) {
            var defaultAvatar = '{{ asset('assets/img/avatar.png') }}';
            $('#avatar').val(null).trigger('change').prop('disabled',true);
            $('#btn_pilih_avatar').prop('disabled', true);
            var output = document.getElementById('avatar_preview');
            output.src = defaultAvatar;
        } else{
            $('#avatar').val(null).trigger('change').prop('disabled',false);
            $('#btn_pilih_avatar').prop('disabled', false);
        }
    }

    function toggleInputPassword(element) {
        var input = element.parents('div.input-group').find('input');
        if (input.prop('type') == 'password') {
            element.find('span').removeClass().addClass('fa fa-eye');
            input.prop('type', 'text');
        } else{
            element.find('span').removeClass().addClass('fa fa-eye-slash');
            input.prop('type', 'password');
        }
    }

</script>
@endpush