@extends('layouts.base')

@include('plugin.jquery-validation')
@include('plugin.datatable')
@include('plugin.select2')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card" id="tabel_card">
            <div class="card-header row">
                <div class="col col-md-2">
                    <button class="btn btn-primary btn-block" type="button" id="btnAdd"><i class="{{ __('label.tambah_icon') }}"></i> Data</button>
                </div>
                <div class="col col-md-6">
                    <div class="card-search with-adv-search dropdown">
                        <form action="javascript:;">
                            <input type="text" id="input_pencarian" autofocus class="form-control" placeholder="{{ __('label.cari_hint') }}" required="">
                            <button type="button" class="btn btn-icon"><i class="{{ __('label.cari_icon') }}"></i></button>
                        </form>
                    </div>
                </div>
                <div class="col col-md-1">
                    <button class="btn btn-outline-dark" type="button" onclick="reloadTable()" ><i class="{{ __('label.muat_ulang_icon') }}"></i> {{ __('label.muat_ulang') }}</button>
                </div>
                <div class="col col-md-3">
                    <div class="card-options text-right" id="length_dropdown">
                        <span class="text-muted text-small mr-2">Tampilkan: </span>
                        <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="current_length">10</span><i class="ik ik-chevron-down mr-0 align-middle"></i>
                        </button>
                        data
                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(1617px, 30px, 0px);">
                            <a class="dropdown-item length_option" data-value="10" data-label="10" href="javascript:;">10</a>
                            <a class="dropdown-item length_option" data-value="20" data-label="25" href="javascript:;">25</a>
                            <a class="dropdown-item length_option" data-value="50" data-label="50" href="javascript:;">50</a>
                            <a class="dropdown-item length_option" data-value="All" data-label="Semua" href="javascript:;">Semua</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="dt-responsive">
                    <table id="tabel" class="table table-inverse table-hover" width="100%">
                        <thead>
                            <tr>
                                <th class="column-checkbox"><label class="custom-control custom-checkbox m-0">
                                        <input type="checkbox" class="custom-control-input" id="select_all" name="select_all" value="">
                                        <span class="custom-control-label">&nbsp;</span>
                                    </label>
                                </th>
                                <th>No.</th>
                                <th>Nama User Group</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer">
                <div class="row" id="aksi_lain">
                    <div class="col col-md-2">
                        <button type="button" class="btn btn-danger btn-block" id="btnHapus" disabled><span class="{{ __('label.hapus_icon') }}"></span> {{ __('label.hapus') }}</button>
                    </div>
                    <div class="col col-md-10">
                        <span class="text-muted text-small mr-2 pt-5 float-right"><b id="data_terpilih">0</b> Data Terpilih</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card" id="form_card" style="display: none">
            <form class="forms-sample" id="form" method="POST" action="javascript:;">
            <div class="card-header"><h6 class="font-weight-bold">Form</h6></div>
            <div class="card-body">
                @csrf
                <input type="hidden" name="user_group_id" id="user_group_id">
                <div class="form-group">
                    <label for="user_group_name">Nama User Group <span class="text-red">*</span></label>
                    <input type="text" class="form-control" name="user_group_name" id="user_group_name" required>
                    <span class="help-block"></span>
                </div>
                
            </div>
            <div class="card-footer">
                <button id="btnSimpan" type="submit" class="btn btn-primary mr-2">{{ __('label.simpan') }}</button>
                <button class="btn btn-danger" type="button" id="btnBack">{{ __('label.batal') }}</button>
            </div>
            </form> 
        </div>
        <div class="card" id="form_manage_group_access_card" style="display: none">
            <form class="forms-sample" id="form_manage_group_access" method="POST" action="javascript:;">
            <div class="card-header"><h3>Kelola Hak Akses Group - <span class="font-weight-bold" id="user_group_name_label"></span></h3></div>
            <div class="card-body">
                @csrf
                <input type="hidden" name="user_group_id_manage" id="user_group_id_manage">
                <table id="tabel_menu" class="table table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">Daftar Menu Aplikasi Web</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
                <button id="btnSimpanAksesGroup" type="submit" class="btn btn-primary mr-2">{{ __('label.simpan') }}</button>
                <button class="btn btn-danger" type="button" id="btnBackAksesGroup">{{ __('label.batal') }}</button>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection

@push('page_script')
<script type="text/javascript">
    var formValidator;

    $(document).ready(function() {

        appScript.combobox();

        $('#tabel').DataTable({
            scrollCollapse: true,
            dom: "t<'row'<'col-md-4'i><'col-md-8'p>>",
            processing: true,
            scrollX:true,
            ajax:{
                url : "{{ route('user_group.load_data') }}",
                type : 'GET',
            },
            language: {
                url: datatableLang
            },
            columnDefs: [
                {visible: false, targets : []},
                {orderable: false, targets : ["column-checkbox",-1]},
                {
                    render: function ( data, type, row ) {
                       return '<label class="custom-control custom-checkbox">\
                                <input type="checkbox" class="custom-control-input select_all_child" id="cb-'+row.no+'" name="cb-'+row.no+'" value="'+data+'">\
                                <span class="custom-control-label">&nbsp;</span>\
                            </label>';
                    },
                    targets: ["column-checkbox"]
                },
                {
                    render: function ( data, type, row ) {
                        var hapus = '<button type="button" title="{{ __('label.hapus') }}" data-toggle="tooltip" class="btn btn-danger hapus"><span class="{{ __('label.hapus_icon') }}"></span></button>';
                        var manageAccess = '<button type="button" title="Kelola Hak Akses Group" data-toggle="tooltip" class="btn '+(row.menus_count > 0 ? 'btn-info' : 'btn-danger')+' kelola_akses"><span class="fa fa-key"></span></button>';
                        var edit = '<button type="button" title="{{ __('label.ubah') }}" data-toggle="tooltip" class="btn btn-primary ubah"><span class="{{ __('label.ubah_icon') }}"></span></button>';
                        return manageAccess+' '+edit;
                    },
                    targets: [-1]
                },
            ],
            order: [[1,"asc"]],
            columns: [
                { width: "3%" , data: "user_group_id"},
                { width: "5%" , data: "no" },
                { width: "70%", data: "user_group_name" },
                { width: "20%", data: "user_group_id" },
            ],
            drawCallback : function(oSettings){
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
                resetAksiLaindanCheckbox();
            },
        });

        $('#length_dropdown').on('click', '.length_option', function(event) {
            var value = $(this).data('value');
            var label = $(this).data('label');

            $('#length_dropdown').find('.current_length').text(label);

            var t = $('#tabel').DataTable();
            var length = value == 'All' ? -1 : value;
            t.page.len(length).draw();
        });

        formValidator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parents("div.form-group").find(".help-block"));
            },
            submitHandler: function(form) {
                saveData();
            }
        });
        
        $("#form_manage_group_access").submit(function(event) {
            saveGroupAccessData();
        });

        $('#tabel tbody').on( 'click', '.ubah', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            showData(data.user_group_id);
        });

        $('#tabel tbody').on( 'click', '.kelola_akses', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            loadGroupAccessMenu(data.user_group_id,data.user_group_name);
        });

        $('#tabel tbody').on( 'click', '.hapus', function () {
            var row = $(this);
            var table = $('#tabel').DataTable();
            var data = table.row( row.parents('tr') ).data();
            appScript.swconfirm("{{ __('message.confirm') }}","{{ __('message.delete_confirm') }}",deleteData,data.user_group_id);
        });

        $('#input_pencarian').on('keyup', function(event) {
            var tabel = $('#tabel');
            tabel.DataTable().search($(this).val()).draw();
        });

        $('#btnAdd').on('click', function(event) {
            openForm();
        });

        $('#btnBack,#btnBackAksesGroup').on('click', function(event) {
            closeForm();
        });

        $('#tabel tbody').on('click', 'input[type="checkbox"].select_all_child', function(event) {
            toggleSelectAllCheckbox();
        });

        $('#select_all').on('click', function(event) {
            $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').prop('checked', $(this).is(':checked'));
            toggleAksiLain();
        });

        $('#btnHapus').on('click', function(event) {
            var selectedUserGroupId = [];
            $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').each(function(index, el) {
                selectedUserGroupId.push($(this).val());
            });

            if (selectedUserGroupId.length > 0) {
                appScript.swconfirm("{{ __('message.confirm') }}","{{ __('message.delete_batch_confirm') }}",deleteBatchData,selectedUserGroupId);
            }
        });
    });

    function toggleSelectAllCheckbox() {
        var checkbox  = $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').length;
        var checkedCB = $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').length;
        $('#select_all').prop('checked', (checkbox == checkedCB));
        toggleAksiLain();
    }

    function toggleAksiLain() {
        var checkbox  = $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').length;
        var checkedCB = $('#tabel tbody tr').find('input[type="checkbox"]:checked.select_all_child').length;
        $('#data_terpilih').text(checkedCB);
        $('#aksi_lain').find('button').prop('disabled',!(checkedCB > 0));
    }

    function resetAksiLaindanCheckbox() {
        $('#tabel tbody tr').find('input[type="checkbox"].select_all_child').prop('checked', false);
        toggleSelectAllCheckbox();
        toggleAksiLain();
    }

    function reloadTable() {
        var t = $('#tabel').DataTable();
        t.ajax.reload();
    }

    function resetForm() {
        formValidator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('.cmb_select2').val('').trigger('change');
        $('input[name="_token"]').val('{{ csrf_token() }}');
    }

    function openForm() {
        resetForm();
        $('#tabel_card').hide();
        $('#form_manage_group_access_card').hide();
        $('#form_card').show();
    }

    function closeForm() {
        $('#form_card').hide();
        $('#form_manage_group_access_card').hide();
        $('#tabel_card').show();
    }

    function showData(userGroupId) {
        $.ajax({
            url: '{{ route('user_group.show') }}',
            type: 'GET',
            dataType: 'JSON',
            data: {
                user_group_id: userGroupId
            },
            success: function(response){
                var data = response.data;

                if (data != null) {
                    openForm();
                    $('#user_group_id').val(data.user_group_id);
                    $('#user_group_name').val(data.user_group_name);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function saveData() {
        $.ajax({
            url: '{{ route('user_group.save') }}',
            type: 'POST',
            dataType: 'JSON',
            data: $('#form').serialize(),
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                if (response.success) {
                    closeForm();
                    reloadTable();
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){
        });
    }

    function deleteData(userGroupId) {
        $.ajax({
            url: '{{ route('user_group.delete') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                user_group_id: userGroupId,
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                reloadTable();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function deleteBatchData(batchUserGroupId) {
        $.ajax({
            url: '{{ route('user_group.delete_batch') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                user_group_id: JSON.stringify(batchUserGroupId)
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                reloadTable();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    //for manage group access
    function resetFormManageGroupAccess() {
        $('#form_manage_group_access')[0].reset();
        $('#form_manage_group_access').find('input[type="hidden"]').val('');
        $('#form_manage_group_access').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#tabel_menu tbody').empty();
        $('#user_group_name_label').empty();
        $('input[name="_token"]').val('{{ csrf_token() }}');
    }

    function openFormManageGroupAccess() {
        resetFormManageGroupAccess();
        $('#tabel_card').hide();
        $('#form_card').hide();
        $('#form_manage_group_access_card').show();
    }

    function loadGroupAccessMenu(userGroupId,userGroupName) {
        $.ajax({
            url: '{{ route('user_group.load_group_access_menu') }}',
            type: 'GET',
            dataType: 'JSON',
            data: {
                user_group_id: userGroupId
            },
            success: function(response){
                openFormManageGroupAccess();

                $("#user_group_name_label").text(userGroupName);
                $("#user_group_id_manage").val(userGroupId);
                var data = response.data;
                var menuHTML = loopMenu(data.menus);
                $('#tabel_menu tbody').html(menuHTML);
                checkGrantedMenu(data.userGroupAccess);
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loopMenu(data, parentMenuId = null, level = 0) {
        var html = '';

        $.each(data, function(index, val) {
            var marginLeft = 25*level;
            var parent = parentMenuId == null ? 'x-' : 'cb_'+parentMenuId;
            html += '<tr>'
            html += '<td><div class="form-group ml-'+(marginLeft)+'">\
            <label class="custom-control custom-checkbox">\
                                <input type="checkbox" class="custom-control-input" value="'+val.menu_id+'" name="cb_'+val.menu_id+'-|'+parent+'-" id="cb_'+val.menu_id+'-" onclick="triggerCheck(this,false)">\
                                <span class="custom-control-label">&nbsp;'+val.menu_name+'</span>\
                            </label>\
                        </div>\
                        </td>';
            html += '</tr>';
            if (val.all_children.length > 0) {
                html += loopMenu(val.all_children,val.menu_id,(level+1));
            }
        });

        return html;
    }

    function checkGrantedMenu(data) {
        $.each(data, function(index, val) {
            var elementId = "cb_"+val.menu_id+"-";
            $("#"+elementId).prop("checked",true);
        });
    }

    function triggerCheck(element, onlyParent = false){
        var name = $(element).attr("name");
        var splitName = name.split("|");
        var id = $(element).attr("id");
        var status = $(element).is(":checked");
        var checkTrue = 0
        //child checked counter
        $("#tabel_menu").find("input[name*='|"+splitName[1]+"']").each(function(){
            if($(this).is(":checked")){
                checkTrue++;
            }
        });
        if(!onlyParent){
            //child check
            $("#tabel_menu").find("input[name*='|"+splitName[0]+"']").each(function(){
                if(status){
                    if(!$(this).is(":checked")) //tidak tercentang
                        $(this).click();
                    else{
                        $(this).prop("checked",false);
                        $(this).click();
                    }
                }
                else{
                    if($(this).is(":checked")) //tercentang
                        $(this).click();
                    else{
                        $(this).prop("checked",true);
                        $(this).click();
                    }
                }
            });
            
        }
        //parents check
        if(checkTrue>0 && status){ //jika ada anaknya tercentang
            $("#tabel_menu").find("input[name*='"+splitName[1]+"|']").each(function(){
                if(!$(this).is(":checked")){ //tidak tercentang
                    $(this).prop("checked",true);
                    triggerCheck($(this),true);
                }
            });
            
        }
        else if(checkTrue==0 && !status){ //jika tidak ada anaknya tercentang
            $("#tabel_menu").find("input[name*='"+splitName[1]+"|']").each(function(){
                if($(this).is(":checked")){ //tercentang
                    $(this).prop("checked",false);
                    triggerCheck($(this),true);
                }
            });
        }
    }


    function saveGroupAccessData() {
        var userGroupId = $('#user_group_id_manage').val();
        var menus       = [];
        $("#tabel_menu").find("input[type='checkbox']").each(function(){
            if($(this).is(":checked")){
                menus.push($(this).val());
            }
        });

        $.ajax({
            url: '{{ route('user_group.save_group_access') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                user_group_id: userGroupId,
                menus: JSON.stringify(menus),
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                if (response.success) {
                    reloadTable();
                    closeForm();
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){
        });
    }


</script>  

@endpush
