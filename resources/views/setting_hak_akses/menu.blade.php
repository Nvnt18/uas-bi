@extends('layouts.base')

@include('plugin.nestable')
@include('plugin.jquery-validation')

@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card" id="tabel_card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h6 class="font-weight-bold">Form Kelola Data Menu</h6><hr>
                        <form class="forms-sample" id="form" method="POST" action="javascript:;">
                            @csrf
                            <input type="hidden" name="menu_id" id="menu_id" value="">
                            <div class="form-group">
                                <label for="menu_name">Nama <span class="text-red">*</span></label>
                                <input type="text" class="form-control" name="menu_name" id="menu_name" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="menu_description">Deskripsi</label>
                                <input type="text" class="form-control" name="menu_description" id="menu_description">
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="menu_url">URL <span class="text-red">*</span></label>
                                <input type="text" class="form-control" name="menu_url" id="menu_url" value="" placeholder="Ex: site/user" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="form-group">
                                <label for="menu_icon">Icon </label>
                                <div class="input-group">
                                    <input type="text" id="menu_icon" name="menu_icon" class="form-control">
                                    <span class="input-group-append">
                                        <label class="input-group-text" id="preview_icon"><span class=""></span></label>
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="alert alert-info" role="alert">
                                <b>Info:</b> 
                                <ul>
                                    <li>Menu Baru yang ditambahkan akan mendapatkan urutan paling akhir.</li>
                                    <li>URL untuk Parent Menu sebaiknya menggunakan <b>"#"</b>.</li>
                                </ul>
                            </div>
                            <hr>
                            <button id="btnSimpan" type="submit" class="btn btn-primary mr-2">{{ __('label.simpan') }}</button>
                            <button class="btn btn-danger" type="button" id="btnReset">{{ __('label.reset') }}</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h6 class="font-weight-bold">Daftar Menu</h6><hr>
                        <button type="button" class="btn btn-outline-dark" onclick="loadData()"><span class="{{ __('label.muat_ulang_icon') }}"></span> {{ __('label.muat_ulang') }}</button>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="dd mt-5" id="menu" style="width: 100%"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('page_script')
<script type="text/javascript">
    
    var formValidator;

    $(document).ready(function() {

        loadData();

        formValidator = $('#form').validate({
            highlight: function(element, errorClass, validClass) {
                $(element).addClass(errorClass).removeClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").addClass(errorClass);
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass(errorClass).addClass(validClass);
                $(element.form).find("label[for=" + element.id + "]").removeClass(errorClass);
            },
            errorClass: "is-invalid text-red",
            errorElement: "em",
            errorPlacement: function(error, element) {
                error.appendTo(element.parents("div.form-group").find(".help-block"));
            },
            submitHandler: function(form) {
                form.submit();
            }
        });
        
        $("#form").submit(function(event) {
            if (formValidator.form()) {
                saveData();
            }
        });

        $('#menu_icon').on('keyup', function(event) {
            var className = $(this).val();
            $('#preview_icon').find('span').removeClass();
            $('#preview_icon').find('span').addClass(className);
        });

        $('#menu').nestable({
            maxDepth: 3,
            group: 1
        });

        $("#menu").on('change', function(event) {
            var serializedStructure = $(this).nestable('serialize');
            updateMenuStructure(serializedStructure)
        });

        $('#btnReset').on('click', function(event) {
            resetForm();
        });
    });

    function resetForm() {
        formValidator.resetForm();
        $('#form')[0].reset();
        $('#form').find('input[type="hidden"]').val('');
        $('input[name="_token"]').val('{{ csrf_token() }}');
        $('#form').find('label,select,input,textarea').removeClass('is-invalid text-red');
        $('#form').find('.cmb_select2').val('').trigger('change');
        $('#preview_icon').find('span').removeClass();
    }

    function loadData() {
        $.ajax({
            url: '{{ route('menu.load_data') }}',
            type: 'GET',
            dataType: 'JSON',
            success: function(response){
                $('#menu').empty();
                $('#menu').html(loopMenu(response.data));
                $('[data-toggle="tooltip"]').tooltip({ boundary: 'window' });
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function loopMenu(data) {
        var html = '<ol class="dd-list">';

        $.each(data, function(index, val) {
            html += '<li class="dd-item dd3-item" data-id="'+val.menu_id+'">\
                            <div class="dd-handle dd3-handle"></div>\
                            <div class="dd3-content" title="URL: '+val.menu_url+'" data-toggle="tooltip"><span class="'+val.menu_icon+'"></span> '+val.menu_name+'<span class="float-right"><a href="javascript:;" class="btn-link" data-menu_id="'+val.menu_id+'" onclick="showData($(this))" role="button" title="{{ __('label.ubah') }}" data-toggle="tooltip"><span class="{{ __('label.ubah_icon') }}"></span></a>&nbsp;&nbsp;&nbsp;<a href="javascript:;" class="btn-link" data-menu_id="'+val.menu_id+'" onclick="deleteData($(this))" role="button" title="{{ __('label.hapus') }}" data-toggle="tooltip"><span class="{{ __('label.hapus_icon') }}"></span></a></span></div>';
            if (val.all_children.length > 0) {
                html += loopMenu(val.all_children);
            }
            html += '</li>';
        });

        html += '</ol>';

        return html;
    }

    function showData(element) {
        resetForm();
        var dataElement = element.data();
        $.ajax({
            url: '{{ route('menu.show') }}',
            type: 'GET',
            dataType: 'JSON',
            data: {
                menu_id: dataElement.menu_id
            },
            success: function(response){
                var data = response.data;

                if (data != null) {
                    $('#menu_id').val(data.menu_id);
                    $('#menu_name').val(data.menu_name);
                    $('#menu_description').val(data.menu_description);
                    $('#menu_url').val(data.menu_url);
                    $('#menu_icon').val(data.menu_icon);
                    $('#preview_icon').find('span').addClass(data.menu_icon);
                } else{
                    appScript.notifikasi('{{ __('message.data_not_found') }}','error');
                }
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        });
    }

    function saveData() {
        $.ajax({
            url: '{{ route('menu.save') }}',
            type: 'POST',
            dataType: 'JSON',
            data: $('#form').serialize(),
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                resetForm();
                loadData();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){
        });
    }

    function deleteData(element) {
        var dataElement = element.data();

        appScript.swconfirm("{{ __('message.confirm') }}","{{ __('message.delete_confirm') }}",function(){
            $.ajax({
                url: '{{ route('menu.delete') }}',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    _token: '{{ csrf_token() }}',
                    menu_id: dataElement.menu_id,
                },
                success: function(response){
                    let jenisNotifikasi = response.success ? 'success' : 'error';
                    appScript.notifikasi(response.message,jenisNotifikasi);
                    loadData();
                },
                error: function(response){
                    appScript.notifikasi('{{ __('message.ajax_error') }}','error');
                }
            }).always(function(response){
            });
        });
    }

    function updateMenuStructure(serializedStructure) {
        $.ajax({
            url: '{{ route('menu.update_menu_structure') }}',
            type: 'POST',
            dataType: 'JSON',
            data: {
                _token: '{{ csrf_token() }}',
                structure: JSON.stringify(serializedStructure)
            },
            success: function(response){
                let jenisNotifikasi = response.success ? 'success' : 'error';
                appScript.notifikasi(response.message,jenisNotifikasi);
                loadData();
            },
            error: function( jqXHR, textStatus, errorThrown ){
                appScript.notifikasi('{{ __('message.ajax_error') }}','error');
            }
        }).always(function(response){
        });
    }

</script>
@endpush