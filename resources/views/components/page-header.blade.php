<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-6">
            <div class="page-header-title">
                @if($menu)
                <i class="{{ $menu->menu_icon }} bg-blue"></i>
                <div class="d-inline">
                    <h5>{{ $menu->menu_name }}</h5>
                    <span>{{ $menu->menu_description }}</span>
                </div>
                @endif
            </div>
        </div>
        <div class="col-lg-6">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <ol class="breadcrumb">
                    {!! $breadcrumb !!}
                </ol>
            </nav>
        </div>
    </div>
</div>
<script>
    var oldTitle = document.title;
    var newTitle = oldTitle+'{{ $menu->menu_name }}';
    document.title = newTitle;
</script>