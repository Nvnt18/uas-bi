<header class="header-top" header-theme="light">
    <div class="container-fluid">
        <div class="d-flex justify-content-between">
            <div class="top-menu d-flex align-items-center">
                <button type="button" class="btn-icon mobile-nav-toggle d-lg-none"><span></span></button>
                <button type="button" id="navbar-fullscreen" class="nav-link"><i class="ik ik-maximize"></i></button>
            </div>
            <div class="top-menu d-flex align-items-center">
                <div class="dropdown">
                    <a class="dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="form-txt-inverse float-left mr-10" style="font-weight: bold; ">
                        {{ $user->name }}<br>
                        <small class="text-right float-right">{{ $user->userGroup->user_group_name }}</small>
                    </span>
                        <img class="avatar" src="{{ $user->avatar ? asset('assets/uploads/avatar/'.$user->avatar) : asset('assets/img/avatar.png') }}" onerror="this.src='{{ asset('assets/img/not-available.jpg') }}'">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="{{ route('profile') }}"><i class="fa fa-user-cog dropdown-icon"></i> Profile</a>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="ik ik-log-out dropdown-icon"></i> Logout</a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</header>