<div class="sidebar-content">
    <div class="nav-container">
        <nav id="main-menu-navigation" class="navigation-main">
           {!! $sideBarMenu !!}
        </nav>
    </div>
</div>
@push('page_script')
<script>
	$('.open-nav').parents('div').each(function(index, el) {
		$(this).addClass('open active');
		$(this).children('a').children('span').eq(1).addClass('open active');
		$(this).children('.sub-menu').css('display', 'block');
	});
</script>
@endpush