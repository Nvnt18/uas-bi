<?php

namespace App\View\Components;

use Illuminate\View\Component;
use App\Models\UserGroup;
use App\Models\Menu;

class Sidebar extends Component
{

    public $currentPage;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        $userGroupId = auth()->user()->user_group_id;
        $sideBarMenu = $this->createSidebarMenu($this->currentPage);

        return view('components.sidebar',compact('sideBarMenu'));
    }

    private function createSidebarMenu($currentPage)
    {
        $userGroupId = auth()->user()->user_group_id;
        $menus = UserGroup::find($userGroupId)->menus()->where('menu_parent_id',null)->orderBy('menu_sequence')->with([
            'allChildrenByUser' => function ($query){
                $query->orderBy('menu_sequence');
            },
        ])->get()->toArray();

        $htmlMenu = $this->loopMenus($menus, $currentPage,true);

        return $htmlMenu;
    }

    /**
     * { Looping Array Of Menus to Create HTML Menu }
     *
     * @param      Array    $menus        The menus
     * @param      String   $currentPage  The current page
     * @param      boolean  $first        The first loop ?
     *
     * @return     string   HTML menu
     */
    private function loopMenus($menus, $currentPage, $first = false)
    {
        $html = '';
        foreach ($menus as $index => $menu) {
            $hasChild = '';
            if (count($menu['all_children_by_user']) > 0) {
                $hasChild = 'has-sub';
            }

            $menuURL = 'javascript:void(0)';
            if ($menu['menu_url'] != '#') {
                $menuURL = url($menu['menu_url']);
            }   

            $icon = '';
            if ($menu['menu_icon'] != '') {
                $icon = '<i class="'.$menu['menu_icon'].'"></i>';
            }

            $navItem = (count($menu['all_children_by_user']) > 0 && $menu['menu_parent_id'] != null) || $first ? 'nav-item' : '';
            $active = $menu['menu_url'] == $currentPage ? "active open open-nav" : "";
            $html .= '<div class="'.$navItem.' '.$active.' '.$hasChild.'"><a href="'.$menuURL.'" class="menu-item '.$active.'" >'.$icon.'<span class="title">'.$menu['menu_name'].'</span></a>';

            //check apakah punya child..
            if (count($menu['all_children_by_user']) > 0) {
                $html .= '<div class="submenu-content">';
                $html .= $this->loopMenus($menu['all_children_by_user'],$currentPage);
                $html .= '</div>';
            }
            $html .= '</div>';
        }

        return $html;
    }
}
