<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Collection;
use App\Models\Menu;

class PageHeader extends Component
{

    public $currentPage;
    private $exceptionPage = ['profile'];

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($currentPage)
    {
        $this->currentPage = $currentPage;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        if (in_array($this->currentPage, $this->exceptionPage)) {
            $menu       = $this->pageInfo($this->currentPage);
            $breadcrumb = $this->createCustomBreadCrumb($this->currentPage);
            return view('components.page-header', compact('menu','breadcrumb'));
        }

        $menu       = Menu::where('menu_url',$this->currentPage)->first();
        $breadcrumb = $this->createBreadCrumb($menu->menu_id,$this->currentPage);
 
        return view('components.page-header', compact('menu','breadcrumb'));
    }

    /**
     * Creates a bread crumb.
     *
     * @param      <type>  $menu         The menu
     * @param      <type>  $currentPage  The current page
     *
     * @return     string  ( description_of_the_return_value )
     */
    private function createBreadCrumb($menuId, $currentPage)
    {
        $html       = '';

        if (!$menuId) {
            return $html;
        }

        $breadcrumb = [];

        $menu = Menu::where('menu_id',$menuId)->with('allParent')->first()->toArray();

        $breadcrumb = $this->loopBreadCrumb($breadcrumb,$menu);
        array_push($breadcrumb,'<i class="ik ik-home"></i>');

        $breadcrumbLength = count($breadcrumb);
        for ($i = $breadcrumbLength; $i > 0; $i--) {
            $active      = $i == 1 ? 'active' : '';
            $ariaCurrent = $i == 1 ? 'aria-current="page"' :'';
            $html .= '<li class="breadcrumb-item '.$active.'" '.$ariaCurrent.'>'.$breadcrumb[($i-1)].'</li>';
        }

        return $html;
    }

    /**
     * { Looping For Create Array of BreadCrumb }
     *
     * @param      array   $array   The current breadcrumb array
     * @param      <type>  $menu    The object of menu
     *
     * @return     array   ( new breadcrumb array )
     */
    private function loopBreadCrumb($array, $menu){

        array_push($array, $menu['menu_name']);
        if($menu['all_parent']){
            $array = $this->loopBreadCrumb($array, $menu['all_parent']);
        }
        return $array;
    }

    function pageInfo($currentPage)
    {
        $pageInfo = new Collection;
        $pageInfo->menu_icon        = '';
        $pageInfo->menu_name        = '';
        $pageInfo->menu_description = '';

        switch ($currentPage) {
            case 'profile':
                $pageInfo->menu_icon        = 'fa fa-user-cog';
                $pageInfo->menu_name        = 'Profile';
                $pageInfo->menu_description = 'Halaman kelola profil user';
                break;
            
            default:
                // code...
                break;
        }

        return $pageInfo;
    }

    function createCustomBreadCrumb($currentPage)
    {
        $breadcrumb = [];
        switch ($currentPage) {
            case 'profile':
                array_push($breadcrumb, 'Profile');
                break;
            
            default:
                // code...
                break;
        }

        array_push($breadcrumb,'<i class="ik ik-home"></i>');
        
        $html       = '';
        $breadcrumbLength = count($breadcrumb);
        for ($i = $breadcrumbLength; $i > 0; $i--) {
            $active      = $i == 1 ? 'active' : '';
            $ariaCurrent = $i == 1 ? 'aria-current="page"' :'';
            $html .= '<li class="breadcrumb-item '.$active.'" '.$ariaCurrent.'>'.$breadcrumb[($i-1)].'</li>';
        }

        return $html;
    }

}
