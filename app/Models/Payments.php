<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $table      = 'payments';
    protected $primaryKey = 'checkNumber';

    public function customers(){
        return $this->belongsTo('App\Models\Customers','customerNumber','customerNumber');
    }
}
