<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table      = 'orders';
	protected $primaryKey = 'orderNumber';

    public function products()
    {
		return $this->belongsToMany('App\Models\Products','orderdetails','orderNumber','productCode');
    }

    public function customers(){
        return $this->belongsTo('App\Models\Customers','customerNumber','customerNumber');
    }
}
