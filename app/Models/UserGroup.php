<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends Model
{

	protected $table      = 'user_groups';
	protected $primaryKey = 'user_group_id';

    protected $fillable = [
        'user_group_name', 'created_by', 'updated_by'
    ];

    public function scopeExcludeRoot($query)
    {
        return $query->where('user_group_type', '<>', 'R');
    }

	public function users()
    {
        return $this->hasMany('App\Models\User','user_group_id','user_group_id');
    }

    function menus()
	{
		return $this->belongsToMany('App\Models\Menu','user_group_access','user_group_id','menu_id');
	}
}
