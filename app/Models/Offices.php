<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offices extends Model
{
    protected $table      = 'offices';
    protected $primaryKey = 'officeCode';
    
    public function employees(){
        return $this->hasMany('App\Models\Employees','officeCode','officeCode');
    }
}
