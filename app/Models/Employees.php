<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $table      = 'employees';
    protected $primaryKey = 'employeeNumber';

    public function offices(){
        return $this->belongsTo('App\Models\Offices','officeCode','officeCode');
    }

    public function reportsTo(){
        return $this->hasMany('App\Models\Employees','reportsTo','employeeNumber');
    }

    public function employees(){
        return $this->belongsTo('App\Models\Employees','reportsTo','employeeNumber');
    }

    public function customers(){
        return $this->hasMany('App\Models\Customers','salesRepEmployeeNumber','employeeNumber');
    }
}
