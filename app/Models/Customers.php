<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table      = 'customers';
    protected $primaryKey = 'customerNumber';

    public function employees(){
        return $this->belongsTo('App\Models\Employees','salesRepEmployeeNumber','employeeNumber');
    }

    public function payments(){
        return $this->hasMany('App\Models\Payments','customerNumber','customerNumber');
    }

    public function orders(){
        return $this->hasMany('App\Models\Orders','customerNumber','customerNumber');
    }
}
