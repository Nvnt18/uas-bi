<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
	protected $table      = 'menus';
	protected $primaryKey = 'menu_id';
	protected $userCrut = null;

	function children()
	{
		return $this->hasMany('App\Models\Menu','menu_parent_id','menu_id');
	}

	public function allChildren()
	{
	    return $this->children()->with('allChildren');
	}

	public function allChildrenByUser()
	{
	    return $this->children()->with('allChildrenByUser')->byUser(auth()->user());
	}

	function scopeByUser($query, $user)
	{
		return $query->join('user_group_access','menus.menu_id', '=', 'user_group_access.menu_id')->where('user_group_id',$user->user_group_id);
	}

	function parent()
	{
		return $this->belongsTo('App\Models\Menu','menu_parent_id','menu_id');
	}

	function allParent()
	{
	    return $this->parent()->with('allParent');
	}

	function userGroups()
	{
		return $this->belongsToMany('App\Models\UserGroup','user_group_access','menu_id','user_group_id');
	}
}
