<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productlines extends Model
{
    protected $table      = 'productlines';
	protected $primaryKey = 'productLine';

    public function products(){
        return $this->hasMany('App\Models\Products','productLine','productLine');
    }
}
