<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table      = 'products';
	protected $primaryKey = 'productCode';

    public function productlines(){
        return $this->belongsTo('App\Models\Productlines','productLine','productLine');
    }

    public function orders()
    {
		return $this->belongsToMany('App\Models\Orders','orderdetails','productCode','orderNumber');
    }
}
