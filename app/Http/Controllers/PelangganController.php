<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use App\Models\Customers;
use App\Models\Payments;
use App\Models\Employees;
use App\Models\Offices;
use Carbon\Carbon;

class PelangganController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
        $year = Orders::selectRaw('distinct(year(orderDate)) as year')->pluck('year');

        $data['year'] = $year;

        return view($this->pathView.'pelanggan',$data);
    }

    function loadDataOrderPelanggan(Request $request){
        $response = self::$response;

        $year = Orders::selectRaw('distinct(year(orderDate)) as year')
            ->orderBy('year','desc')
            ->limit(5)
            ->get();
        $year = $year->reverse();

        $data = [];
        $categories = [];
        foreach ($year as $key => $value) {
            $val = Orders::selectRaw('count(distinct(customerNumber)) as pelanggan')
                ->whereRaw('year(orderDate) = '.$value->year)
                ->pluck('pelanggan');
                
            $data[sizeOf($year) - 1 - $key] = $val[0];
            $categories[sizeOf($year) - 1 - $key] = $value->year;
        }

        $response['success'] = true;
        $response['data']       = $data;
        $response['categories']   = $categories;

        // dd($response);

        return response()->json($response);
    }

    function loadDataComparisonChart(Request $request){
        $response = self::$response;
        
        $dataNow = [];
        $dataPrev = [];
        $dataCountry = Customers::selectRaw('distinct(country) as name')->get()->toArray();

        $year = Orders::selectRaw('distinct(year(orderDate)) as year')->get();
        $dataYear = [];

        foreach($year as $key => $value){
            $orders = Customers::groupBy('customers.country')
                ->selectRaw('distinct(customers.country) as negara, 
                    (select count(orders.orderNumber) as numOrders
                    from customers join orders on customers.customerNumber = orders.customerNumber
                    where customers.country = negara and year(orderDate) = '.$value->year.'
                    group by customers.country) as numOrders')
                ->orderBy('country','asc')
                ->get();

            $dataOrdersCountry = [];
            foreach($orders as $key2 => $value2){
                $array_data[0] = $value2->negara;
                $array_data[1] = $value2->numOrders ? $value2->numOrders : 0;
                $dataOrdersCountry[$key2] = $array_data;
            }

            if($key != 0){
                $dataNow[$value->year] = $dataOrdersCountry;
                $dataYear[$key-1] = $value->year;
            }
            if($key != sizeOf($year)-1){
                $dataPrev[$value->year+1] = $dataOrdersCountry;
            }

            if($key == sizeOf($year)-1){
                $yearNow = $value->year;
            }
        }

        $response['success'] = true;
        $response['dataCountry'] = $dataCountry;
        $response['dataNow'] = $dataNow;
        $response['dataPrev'] = $dataPrev;
        $response['yearNow'] = $yearNow;
        $response['year'] = $dataYear;
        
        return response()->json($response);
    }

    function loadOnTime(Request $request){
        $response = self::$response;
        
        $year = $request->date ? Carbon::createFromFormat('Y',$request->date)->format('Y') : null;

        $on_time = Orders::whereRaw('shippedDate <= requiredDate and year(orderDate) = '.$year)
            ->count('orderNumber');
        
        $late = Orders::whereRaw('shippedDate > requiredDate and year(orderDate) = '.$year)
            ->count('orderNumber');

        if($on_time >= (($on_time + $late)*0.9) && $on_time != 0){
            $response['status']    = '<span class="badge badge-default">Target: 90%</span>&nbsp;<span class="badge badge-success">Status: Good</span>';
        }else{
            $response['status']    = '<span class="badge badge-default">Target: 90%</span>&nbsp;<span class="badge badge-danger">Status: Bad</span>';
        }
        
        $response['success'] = true;
        $response['on_time'] = $on_time;
        $response['late']    = $late;

        return response()->json($response);
    }
}
