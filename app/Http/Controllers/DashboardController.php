<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use Phpml\Association\Apriori;

class DashboardController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $isPic = false;

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
        $users = User::where('type','C')->where('status','A')->get();

        $size = count($users);

        $dataUsers = [];

        if($size ==0){
            $dataUsers['name']      = "Tidak ada anggota";
            $dataUsers['avatar']  = null;
            $data['status']  = 0;
            $data['users'] = $dataUsers;
        }else{
            foreach ($users as $key => $value) {
                $dataUsers[$key]['no']     = ($key+1);
                $dataUsers[$key]['name']     = $value->name;
                $dataUsers[$key]['avatar']   = $value->avatar ? $value->avatar : null;
            }
            $data['status']  = 1;
            $data['users'] = $dataUsers;
        }

    	return view($this->pathView.'dashboard', $data);
    }
}
