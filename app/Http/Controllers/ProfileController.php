<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\User;

class ProfileController extends Controller
{
    private static $response = [
        'success' => false,
        'data'    => null,
        'message' => null
    ];

    public function index()
    {
    	$profile = auth()->user();
    	return view('profile', compact('profile'));
    }

    function save(Request $request)
    {
        $response = self::$response;

    	Validator::make($request->all(), [
            'name'             => 'required|max:255',
            'password'         => 'nullable|alpha_num|min:6',
            'password_confirm' => 'nullable|same:password',
            'avatar'           => 'nullable|image|max:2048'
		])->validate();

        $user = User::find($request->id);

        if (!$user || $request->id != auth()->user()->id) {
            $response['success'] = false;
            $response['message'] = 'ID user tidak valid';
            return response()->json($response);
        }

        $user->name       = $request->name;
        $user->updated_by = auth()->user()->id;

        if ($request->ubah_password) {
            $user->password = $request->password;
        }

        if ($request->reset_avatar) {
            $user->avatar = null;
        } else{
            if ($request->hasFile('avatar')) {
                $uniqueStamp = substr(md5(microtime()), -8);
                $upload    = auth()->user()->email . '_' . date('Y_m_d') . '_' . $uniqueStamp . '.'. $request->avatar->getClientOriginalExtension();
                if ($request->file('avatar')->isValid()) {
                    $request->avatar->move(public_path('assets/uploads/avatar'), $upload); // upload to public directory
                    $user->avatar = $upload; //set filename to database column
                }
            }
        }

        $save = $user->save();
        $response['success'] = $save;
        $response['message'] = $save ? __('message.save_data_success') : __('message.save_data_failed');

    	return response()->json($response);
    }
}
