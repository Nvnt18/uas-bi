<?php

namespace App\Http\Controllers\SettingHakAkses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserGroup;
use App\Models\Menu;

class UserGroupController extends Controller
{
	private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    public function index()
    {
    	return view('setting_hak_akses.user_group');
    }

    function loadData()
    {
    	$response = self::$response;

    	$userGroups = auth()->user()->type == 'R' ? UserGroup::withCount('menus')->get() : UserGroup::excludeRoot()->withCount('menus')->get();

        $data = [];

        foreach ($userGroups as $key => $value) {
            $data[$key]['no']              = ($key+1);
            $data[$key]['user_group_name'] = $value->user_group_name;
            $data[$key]['user_group_id']   = $value->user_group_id;
            $data[$key]['menus_count']     = $value->menus_count;
        }

        $response['success'] = true;
        $response['data']    = $data;

    	return response()->json($response);
    }

    function combobox()
    {
        $response = self::$response;

        $userGroups = auth()->user()->type == 'R' ? UserGroup::all() : UserGroup::excludeRoot()->get();

        $data = [];

        foreach ($userGroups as $key => $value) {
            $data[$key]['id']   = $value->user_group_id;
            $data[$key]['name'] = $value->user_group_name;
        }

        $response['success'] = true;
        $response['data']    = $data;

        return response()->json($response);
    }

    function show(Request $request)
    {
        $response = self::$response;

        $userGroup = UserGroup::find($request->query('user_group_id'));

        $response['success'] = ($userGroup != null);
        $response['data']    = $userGroup;
        return response()->json($response);
    }

    function save(Request $request)
    {
        $response = self::$response;
        
        $save = false;

        $userGroup = UserGroup::find($request->user_group_id);

        if (!$userGroup) {
            $userGroup = new UserGroup;
            $userGroup->user_group_name = $request->user_group_name;
            $userGroup->created_by      = auth()->user()->id;

            $save = $userGroup->save();
        } else{

            $userGroup->user_group_name = $request->user_group_name;
            $userGroup->updated_by      = auth()->user()->id;

            $save = $userGroup->save();
        }

        $response['success'] = $save;
        $response['message'] = $save ? __('message.save_data_success') : __('message.save_data_failed');

        return response()->json($response);
    }

    function delete(Request $request)
    {
        $response = self::$response;
        try {
            $delete = UserGroup::destroy($request->user_group_id);

            $response['success'] = true;
            $response['message'] = __('message.delete_data_success');

            return response()->json($response);
            
        } catch (\Exception $e) {
            $response['message'] = __('message.delete_data_failed');
            return response()->json($response);
        }

    }

    function deleteBatch(Request $request)
    {
        $response = self::$response;

        try {
            
            $batchUserGroupId = json_decode($request->user_group_id);

            $delete = UserGroup::destroy($batchUserGroupId);

            $response['success'] = true;
            $response['message'] = __('message.delete_data_success');

            return response()->json($response);
            
        } catch (\Exception $e) {
            $response['message'] = __('message.delete_data_failed');
            return response()->json($response);
        }

    }

    //for manage group access
    function loadGroupAccessMenu(Request $request)
    {
        $response = self::$response;

        $userGroupId = $request->query('user_group_id');

        $userGroupAccess = UserGroup::find($userGroupId)->menus;

        $menus = Menu::where('menu_parent_id',null)->with(['allChildren'=> function($query){
            $query->orderBy('menu_sequence');
        }])->orderBy('menu_sequence')->get();

        $response['success'] = true;
        $response['data']    = compact('menus','userGroupAccess');
        return response()->json($response);
        
    }

    function saveGroupAccess(Request $request)
    {
        $response = self::$response;
        
        $userGroup = UserGroup::find($request->user_group_id);
        $menus     = json_decode($request->menus);

        if ($userGroup) {
            $save = $userGroup->menus()->sync($menus);

            $response['success'] = $save;
            $response['message'] = $save ? __('message.save_data_success') : __('message.save_data_failed');
        } else{
            $response['success'] = false;
            $response['message'] = __('message.data_not_found');
        }

        return response()->json($response);
    }
}
