<?php

namespace App\Http\Controllers\SettingHakAkses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Menu;

class MenuController extends Controller
{

	private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    public function index()
    {
    	return view('setting_hak_akses.menu');
    }

    function loadData()
    {
        $response = self::$response;

        $menus = Menu::where('menu_parent_id',null)->with(['allChildren'=> function($query){
            $query->orderBy('menu_sequence');
        }])->orderBy('menu_sequence')->get();

        $response['success'] = true;
        $response['data']    = $menus;

    	return response()->json($response);
    }

    function show(Request $request)
    {
        $response = self::$response;

        $menu = Menu::find($request->query('menu_id'));

        $response['success'] = $menu != null ? true : false;
        $response['data']    = $menu;
        return response()->json($response);
        
    }

    function save(Request $request)
    {
        $response = self::$response;
        
        $save = false;

        $menu = Menu::find($request->menu_id);

        if (!$menu) {
            $menu = new Menu;
            $menu->menu_name        = $request->menu_name;
            $menu->menu_url         = $request->menu_url;
            $menu->menu_description = $request->menu_description;
            $menu->menu_icon        = $request->menu_icon;
            $menu->menu_sequence    = Menu::where('menu_parent_id',null)->max('menu_sequence')+1;
            $menu->created_by       = auth()->user()->id;

            $save = $menu->save();
        } else{

            $menu->menu_name        = $request->menu_name;
            $menu->menu_url         = $request->menu_url;
            $menu->menu_description = $request->menu_description;
            $menu->menu_icon        = $request->menu_icon;
            $menu->updated_by       = auth()->user()->id;

            $save = $menu->save();
        }

        $response['success'] = $save;
        $response['message'] = $save ? __('message.save_data_success') : __('message.save_data_failed');

        return response()->json($response);
    }

    function delete(Request $request)
    {
        $response = self::$response;

        try {
            $delete = Menu::destroy($request->menu_id);

            $response['success'] = true;
            $response['message'] = __('message.delete_data_success');
            return response()->json($response);
            
        } catch (\Exception $e) {
            
            $response['message'] = __('message.delete_data_failed');
            return response()->json($response);
        }

    }

    function updateMenuStructure(Request $request)
    {
        $response = self::$response;
        
        $structure = json_decode($request->structure);

        try {
            DB::transaction(function () use ($structure) {
                $this->loopUpdateStructure($structure);
            });

            $response['success'] = true;
            $response['message'] = __('message.update_data_success');

            return response()->json($response);

        } catch (\Exception $e) {

            $response['message'] = __('message.update_data_failed');

            return response()->json($response);
        }
    }

    private function loopUpdateStructure($structure,$parentId = null)
    {
        foreach ($structure as $key => $value) {

            $menu = Menu::findOrFail($value->id);

            $menu->updated_by     = auth()->user()->id;
            $menu->menu_parent_id = $parentId;
            $menu->menu_sequence  = ($key+1);
            $menu->save();

            if (isset($value->children)) {
                $this->loopUpdateStructure($value->children,$value->id);
            }
        }
    }
}
