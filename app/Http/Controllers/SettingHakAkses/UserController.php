<?php

namespace App\Http\Controllers\SettingHakAkses;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    public function index()
    {
    	return view('setting_hak_akses.user');
    }

    function loadData(Request $request)
    {
    	$response = self::$response;

    	$users = auth()->user()->type == 'R' ? User::select('id','name','email','status','user_group_name')->join('user_groups','users.user_group_id','user_groups.user_group_id') : User::select('id','name','email','status','user_group_name')->join('user_groups','users.user_group_id','user_groups.user_group_id')->excludeRoot();

        if ($request->user_group_id && $request->user_group_id != 'ALL') {
            $users->where('users.user_group_id',$request->user_group_id);
        }

        $data = [];

        foreach ($users->get() as $key => $value) {
            $data[$key]['no']              = ($key+1);
            $data[$key]['name']            = $value->name;
            $data[$key]['email']           = $value->email;
            $data[$key]['user_group_name'] = $value->user_group_name;
            $data[$key]['status']          = $value->status;
            $data[$key]['id']              = $value->id;
        }

        $response['success'] = true;
        $response['data']    = $data;

    	return response()->json($response);
    }

    function show(Request $request)
    {
        $response = self::$response;

        $user = User::find($request->query('id'));

        $response['success'] = ($user != null);
        $response['data']    = $user;
        return response()->json($response);
    }

    function save(Request $request)
    {
        $response = self::$response;
        
        $save = false;

        $user = User::find($request->id);

        if (!$user) {
            $user = new User;
            $user->name          = $request->name;
            $user->email         = $request->email;
            $user->password      = $request->password;
            $user->user_group_id = $request->user_group_id;
            $user->created_by    = auth()->user()->id;

            $save = $user->save();
        } else{
            if($request->ubah_password)
                $user->password      = $request->password;
            
            $user->name          = $request->name;
            $user->email         = $request->email;
            $user->user_group_id = $request->user_group_id;
            $user->updated_by    = auth()->user()->id;

            $save = $user->save();
        }

        $response['success'] = $save;
        $response['message'] = $save ? __('message.save_data_success') : __('message.save_data_failed');

        return response()->json($response);
    }

    function updateStatusBatch(Request $request)
    {
        $response = self::$response;

        try {
            
            $batchUserId = json_decode($request->id);

            $dataUpdate = [
                'status' => $request->status,
                'updated_by' => auth()->user()->id
            ];

            $update = User::whereIn('id',$batchUserId)->update($dataUpdate);

            $response['success'] = true;
            $response['message'] = __('message.update_data_success');

            return response()->json($response);
            
        } catch (\Exception $e) {
            $response['message'] = __('message.update_data_failed');
            return response()->json($response);
        }
    }

    function delete(Request $request)
    {
        $response = self::$response;
        try {
            $delete = User::destroy($request->id);

            $response['success'] = true;
            $response['message'] = __('message.delete_data_success');

            return response()->json($response);
            
        } catch (\Exception $e) {
            $response['message'] = __('message.delete_data_failed');
            return response()->json($response);
        }

    }

    function deleteBatch(Request $request)
    {
        $response = self::$response;

        try {
            
            $batchuserId = json_decode($request->id);

            $delete = User::destroy($batchuserId);

            $response['success'] = true;
            $response['message'] = __('message.delete_data_success');

            return response()->json($response);
            
        } catch (\Exception $e) {
            $response['message'] = __('message.delete_data_failed');
            return response()->json($response);
        }
    }


    function checkEmailAvailable(Request $request)
    {
        $id    = $request->id;
        $email = $request->email;

        if ($id) {
            $checkResult = User::where([
                ['email', $email],
                ['id', '<>', $id]
            ])->count();

            $result = $checkResult > 0 ? "Email Sudah Digunakan" : "true";

            return response()->json($result);
        }

        $checkResult = User::where('email', $email)->count();

        $result = $checkResult > 0 ? "Email Sudah Digunakan" : "true";

        return response()->json($result);
    }
}
