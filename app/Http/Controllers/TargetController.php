<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TargetController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
    	return view($this->pathView.'target');
    }
}
