<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use App\Models\Customers;
use App\Models\Payments;
use App\Models\Employees;
use App\Models\Offices;
use Carbon\Carbon;

class InternalController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
    	return view($this->pathView.'internal');
    }

    function loadOffice(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $office = Offices::selectRaw('offices.city as of_city, offices.country as of_country, (select count(orders.orderNumber) from offices join employees on offices.officeCode = employees.officeCode join customers on employees.employeeNumber = customers.salesRepEmployeeNumber join orders on customers.customerNumber = orders.customerNumber where month(orders.orderDate) = '.$month.' and year(orders.orderDate) = '.$year.' and offices.city = of_city and offices.country = of_country) as orderGet')
            ->orderBy('orderGet','desc')
            ->get();

        $categories = [];
        $data = [];
        foreach ($office as $key => $value) {
            $categories[$key] = $value->of_city.', '.$value->of_country;
            $data[$key] = $value->orderGet;
        }

        $response['success']    = true;
        $response['categories'] = $categories;
        $response['data']       = $data;

        return response()->json($response);
    }

    function loadProduct(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $productlines = Productlines::selectRaw('productlines.productLine as name,
            (select sum(orderdetails.quantityOrdered) from productlines join products on productlines.productLine = products.productLine join orderdetails on products.productCode = orderdetails.productCode join orders on orderdetails.orderNumber = orders.orderNumber where productlines.productLine = name and year(orders.orderDate) = '.$year.' and month(orders.orderDate) = '.$month.') as y,
            productlines.productLine as drilldown')
            ->get();
        
        $drilldown = [];
        $productlines_data = [];
        foreach ($productlines as $key => $value) {
            $product = Products::join('orderdetails','orderdetails.productCode','products.productCode')
                ->join('orders','orderdetails.orderNumber','orders.orderNumber')
                ->groupBy('products.productName')
                ->selectRaw('products.productName, sum(orderdetails.quantityOrdered) as productTerjual')
                ->whereRaw('products.productLine = "'.$value->name.'" and year(orders.orderDate) = '.$year.' and month(orders.orderDate) = '.$month)
                ->get();

            $drilldown[$key]['name'] = $value->name;
            $drilldown[$key]['id']   = $value->name;
            $product_data = [];
            foreach ($product as $key2 => $value2) {
                $data[0] = $value2->productName;
                $data[1] = (int)$value2->productTerjual;
                $product_data[$key2] = $data;
            }
            $drilldown[$key]['data'] = $product_data;

            $productlines_data[$key]['name']        = $value->name;
            $productlines_data[$key]['y']           = (int)$value->y;
            $productlines_data[$key]['drilldown']   = $value->drilldown;
        }

        $response['success']        = true;
        $response['productlines']   = $productlines_data;
        $response['drilldown']      = $drilldown;

        // dd($response);

        return response()->json($response);
    }

    function loadStatus(Request $request){
        $response = self::$response;

        $year = $request->date ? Carbon::createFromFormat('Y',$request->date)->format('Y') : null;

        $status = Orders::selectRaw('distinct(status) sts, 
            (select count(status) from orders where year(orderDate) = '.$year.' and status = sts group by status) as jumlah')
            ->get();

        $data = [];
        $jumlah = 0;
        $good = 0;
        foreach ($status as $key => $value) {
            $data[$key]['name'] = $value->sts;
            $data[$key]['y'] = $value->jumlah;
            if($value->sts == 'Shipped'){
                $data[$key]['color'] = '#2dce89';
                $good += $value->jumlah;
            }else if($value->sts == 'In Process'){
                $data[$key]['color'] = '#007bff';
            }else if($value->sts == 'On Hold'){
                $data[$key]['color'] = '#fb6340';
            }else if($value->sts == 'Disputed'){
                $data[$key]['color'] = '#343a40';
            }else if($value->sts == 'Resolved'){
                $data[$key]['color'] = '#11cdef';
            }else if($value->sts == 'Cancelled'){
                $data[$key]['color'] = '#f5365c';
            }

            $jumlah += $value->jumlah;
        }

        if($good >= ($jumlah*0.8) && $good != 0 && $jumlah != 0){
            $response['status']    = '<span class="badge badge-default">Target: 80%</span>&nbsp;<span class="badge badge-success">Status: Good</span>';
        }else{
            $response['status']    = '<span class="badge badge-default">Target: 80%</span>&nbsp;<span class="badge badge-danger">Status: Bad</span>';
        }

        $response['success']    = true;
        $response['data']       = $data;

        return response()->json($response);
    }
}
