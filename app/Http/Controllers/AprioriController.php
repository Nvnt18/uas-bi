<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use App\Models\Customers;
use App\Models\Payments;
use App\Models\Employees;
use App\Models\Offices;
use Phpml\Association\Apriori;

class AprioriController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
    	return view($this->pathView.'apriori');
    }

    function loadDataSetApriori(Request $request){
        $response = self::$response;

        $samples =[];
        $labels  = [];
        $itemSet = [];
        $dataAntecedent = [];
        $dataConsequent = [];
        $dataSupport = [];
        $dataConfidence = [];
        
        $associator = new Apriori();

        if($request->support || $request->confidence){
            if($request->support && $request->confidence){
                $associator = new Apriori($request->support/100, $request->confidence/100);
            }else if($request->support){
                $associator = new Apriori($request->support/100, 0);
            }
            else if($request->confidence){
                $associator = new Apriori(0, $request->confidence/100);
            }
        }
        
        $orders = Orders::select('orderNumber')->get();

        foreach ($orders as $key => $value) {
            $samples[$key] = Products::selectRaw('distinct(products.productLine)')
            ->join('orderdetails','orderdetails.productCode','products.productCode')
            ->where('orderdetails.orderNumber',$value->orderNumber)
            ->pluck('productLine')->toArray();
        }
        
        $associator->train($samples, $labels);

        foreach($associator->getRules() as $key => $value){

            $html ='';
            $antecedent = '';
            $consequent = '';
            
            foreach($associator->getRules()[$key]['antecedent'] as $i => $item){
                $html .= '<span class="badge badge-primary">'.$item.'</span>&nbsp;';
                $antecedent .= $item.', ';
            }

            $html .= '<i class="fa fa-arrow-right" aria-hidden="true"></i>&nbsp;';

            foreach($associator->getRules()[$key]['consequent'] as $i => $item){
                $html .= '<span class="badge badge-dark">'.$item.'</span>&nbsp;';
                $consequent .= $item.', ';
            }

            $indicator = '';
            if(round($value['support'] * 100, 2) >= $request->support && round($value['confidence'] * 100, 2) >= $request->confidence){
                $indicator .= '<span class="badge badge-success">Memenuhi</span>';
            }else{
                $indicator .= '<span class="badge badge-danger">Tidak Memenuhi</span>';
            }
            
            $itemSet[$key]['no']            = ($key+1);
            $itemSet[$key]['item']          = $html;    
            $itemSet[$key]['support']       = round($value['support'] * 100, 2)."%";
            $itemSet[$key]['confidence']    = round($value['confidence'] * 100, 2)."%";  
            $itemSet[$key]['indicators']    = $indicator;

            $dataAntecedent[$key]           = $antecedent;  
            $dataConsequent[$key]           = $consequent;
            $dataSupport[$key]              = round($value['support']*-100, 2);
            $dataConfidence[$key]           = round($value['confidence']*100, 2);
        }

        $response['success'] = true;
        $response['data'] = $itemSet;
        $response['dataAntecedent'] = $dataAntecedent;
        $response['dataConsequent'] = $dataConsequent;
        $response['dataSupport'] = $dataSupport;
        $response['dataConfidence'] = $dataConfidence;
        $response['size'] = sizeOf($dataConfidence);

        // dd($response);

    	return response()->json($response);
    }

    function checkPersentase(Request $request)
    {
        $persentase    = $request->persentase;

        $result = "true";

        if($persentase <= 0 || $persentase > 100){
            $result = $persentase <= 0 || $persentase > 100 ? "Persentase Harus 0-100 %" : "true";
        }

        return response()->json($result);
    }

}
