<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use App\Models\Customers;
use App\Models\Payments;
use App\Models\Employees;
use App\Models\Offices;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class KeuanganController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
    	return view($this->pathView.'keuangan');
    }

    function loadIncome(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $income = Payments::whereRaw('month(paymentDate) = '.$month.' and year(paymentDate) = '.$year)
            ->sum('amount');
        
        $target = 200000;

        $response['success'] = true;
        $response['text'] = (double)$income;
        $response['target_income'] = $target;

        $point = 0;

        if($income > $target){
            $point = $target;
        }else{
            $point = $income;
        }

        $response['point'] = (double)$point;

        $badge = '';

        if((double)$income >= 0 && (double)$income <= ($target * 0.3)){
            $badge = '<span class="badge badge-danger">';
        }else if((double)$income > ($target * 0.3) && (double)$income <= ($target * 0.7)){
            $badge = '<span class="badge badge-warning">';
        }else if((double)$income > ($target * 0.7)){
            $badge = '<span class="badge badge-success">';
        }

        $response['badge'] = $badge;
        $response['title'] = '<span class="badge badge-danger"><'.($target * 0.3).'</span>&nbsp;<span class="badge badge-warning">'.($target * 0.3).'-'.($target * 0.7).'</span>&nbsp;<span class="badge badge-success">>'.($target * 0.7).'</span>';

        return response()->json($response);
    }

    function loadNetProfit(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $net_profit = Products::join('orderdetails','orderdetails.productCode','products.productCode')
            ->join('orders','orderdetails.orderNumber','orders.orderNumber')
            ->selectRaw('sum((orderdetails.priceEach-products.buyPrice)*orderdetails.quantityOrdered) as profit')
            ->whereRaw('orders.orderNumber in (select orders.orderNumber from orderdetails join orders on orderdetails.orderNumber = orders.orderNumber join customers on orders.customerNumber = customers.customerNumber join payments on customers.customerNumber = payments.customerNumber where month(orders.orderDate) = '.$month.' and year(orders.orderDate) = '.$year.' and month(orders.orderDate) = month(payments.paymentDate) group by customers.customerNumber, orders.orderDate,payments.paymentDate, payments.amount,orders.orderNumber having sum(orderdetails.priceEach*orderdetails.quantityOrdered) = payments.amount)')
            ->pluck('profit');

        $target = 100000;

        $response['success'] = true;
        $response['text'] = (double)$net_profit[0];
        $response['target_net_profit'] = $target;

        $point = 0;

        if($net_profit[0] > $target){
            $point = $target;
        }else{
            $point = (double)$net_profit[0];
        }

        $response['point'] = (double)$point;

        $badge = '';

        if((double)$net_profit[0] >= 0 && (double)$net_profit[0] <= ($target * 0.3)){
            $badge = '<span class="badge badge-danger">';
        }else if((double)$net_profit[0] > ($target * 0.3) && (double)$net_profit[0] <= ($target * 0.7)){
            $badge = '<span class="badge badge-warning">';
        }else if((double)$net_profit[0] > ($target * 0.7)){
            $badge = '<span class="badge badge-success">';
        }

        $response['badge'] = $badge;
        $response['title'] = '<span class="badge badge-danger"><'.($target * 0.3).'</span>&nbsp;<span class="badge badge-warning">'.($target * 0.3).'-'.($target * 0.7).'</span>&nbsp;<span class="badge badge-success">>'.($target * 0.7).'</span>';

        return response()->json($response);
    }

    function loadOrders(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $orders = Orders::join('orderdetails','orderdetails.orderNumber','orders.orderNumber')
            ->selectRaw('sum(orderdetails.quantityOrdered*orderdetails.priceEach) as orders')
            ->whereRaw('month(orders.orderDate) between 01 and '.$month.' and year(orderDate) = '.$year)
            ->pluck('orders');

        $lunas = Payments::whereRaw('month(payments.paymentDate) between 01 and '.$month.' and year(payments.paymentDate) = '.$year)
            ->sum('amount');
        
        $badge = '';

        if((double)round($lunas,2) >= ((double)round($orders[0],2) * 0.7) && (double)round($orders[0],2) != 0){
            $badge = '<span class="badge badge-default">Target 70%</span><br/><span class="badge badge-success">Status: Good</span>';
        }else{
            $badge = '<span class="badge badge-default">Target 70%</span><br/><span class="badge badge-danger">Status: Bad</span>';
        }
        
        $response['success'] = true;
        $response['lunas'] = (double)round($lunas,2);
        $response['belum'] = (double)round($orders[0]-$lunas,2);
        $response['full_orders'] = (double)round($orders[0],2);
        $response['badge'] = $badge;

        return response()->json($response);
    }
}
