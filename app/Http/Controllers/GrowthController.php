<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Orders;
use App\Models\User;
use App\Models\Products;
use App\Models\Productlines;
use App\Models\Customers;
use App\Models\Payments;
use App\Models\Employees;
use App\Models\Offices;
use Carbon\Carbon;

class GrowthController extends Controller
{
    private static $response = [
		'success' => false,
		'data'    => null,
		'message' => null
	];

    private $pathView = 'backend.administrator.';

    function index(Request $request)
    {
    	return view($this->pathView.'growth');
    }

    function loadData(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $sales = Offices::join('employees','employees.officeCode','offices.officeCode')
            ->selectRaw('employees.employeeNumber as id, employees.firstName, employees.lastName, employees.jobTitle, offices.city, offices.country, (select count(orders.orderNumber) from customers join orders on customers.customerNumber = orders.customerNumber where month(orders.orderDate) = '.$month.' and year(orders.orderDate) = '.$year.' and customers.salesRepEmployeeNumber = id group by customers.salesRepEmployeeNumber) as customerGet')
            ->where('jobTitle','sales rep')
            ->orderBy('customerGet','desc')
            ->get();

        $data = [];
        foreach($sales as $key => $value){

            $data[$key]['no']               = ($key+1);
            $data[$key]['id']               = $value->id;    
            $data[$key]['nama']             = $value->firstName.' '.$value->lastName;
            $data[$key]['jabatan']          = $value->jobTitle;  
            $data[$key]['cabang']           = $value->city.', '.$value->country;
            $data[$key]['jumlah_pelanggan'] = $value->customerGet ? $value->customerGet : 0;
        }

        $response['success']    = true;
        $response['data']       = $data;

        return response()->json($response);
    }

    function loadSales(Request $request){
        $response = self::$response;

        $month = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('m') : null;
        $year = $request->date ? Carbon::createFromFormat('m-Y',$request->date)->format('Y') : null;

        $sales = Offices::join('employees','employees.officeCode','offices.officeCode')
            ->selectRaw('employees.employeeNumber as id, employees.firstName, employees.lastName, employees.jobTitle, offices.city, offices.country, (select count(orders.orderNumber) from customers join orders on customers.customerNumber = orders.customerNumber where month(orders.orderDate) = '.$month.' and year(orders.orderDate) = '.$year.' and customers.salesRepEmployeeNumber = id group by customers.salesRepEmployeeNumber) as customerGet')
            ->where('jobTitle','sales rep')
            ->orderBy('customerGet','desc')
            ->get();
        
        $drilldown = [];
        $target = 1;
        $index_good = 0;
        $index_bad = 0;
        $good_or_bad_data = [];
        $drilldown[0]['name'] = "Terpenuhi";
        $drilldown[0]['id']   = "Terpenuhi";
        $drilldown[0]['data'] = [];

        $drilldown[1]['name'] = "Tidak Terpenuhi";
        $drilldown[1]['id']   = "Tidak Terpenuhi";
        $drilldown[1]['data'] = [];
        foreach ($sales as $key => $value) {
            if($value->customerGet >= $target){
                $drilldown[0]['name'] = "Terpenuhi";
                $drilldown[0]['id']   = "Terpenuhi";
                $data[0] = $value->firtsName.' '.$value->lastName;
                $data[1] = $value->customerGet ? $value->customerGet : 0;
                $sales_data[$index_good] = $data;
                $drilldown[0]['data'] = $sales_data;

                $index_good++;
            }else{
                $drilldown[1]['name'] = "Tidak Terpenuhi";
                $drilldown[1]['id']   = "Tidak Terpenuhi";
                $data[0] = $value->firtsName.' '.$value->lastName;
                $data[1] = $value->customerGet ? $value->customerGet : 0;
                $sales_data[$index_bad] = $data;
                $drilldown[1]['data'] = $sales_data;

                $index_bad++;
            }
        }

        $good_or_bad_data[0]['name']        = "Terpenuhi";
        $good_or_bad_data[0]['y']           = $index_good;
        $good_or_bad_data[0]['drilldown']   = "Terpenuhi";
        $good_or_bad_data[0]['color']       = "#2dce89";

        $good_or_bad_data[1]['name']        = "Tidak Terpenuhi";
        $good_or_bad_data[1]['y']           = $index_bad;
        $good_or_bad_data[1]['drilldown']   = "Tidak Terpenuhi";
        $good_or_bad_data[1]['color']       = "#f5365c";

        $response['success']        = true;
        $response['good_or_bad_data']   = $good_or_bad_data;
        $response['drilldown']      = $drilldown;

        if($index_good >= (($index_good + $index_bad)*0.7) && $index_good != 0 && ($index_good + $index_bad) != 0){
            $response['title']    = '<span class="badge badge-default">Target: 70%</span>&nbsp;<span class="badge badge-success">Status: Good</span>';
        }else{
            $response['title']    = '<span class="badge badge-default">Target: 70%</span>&nbsp;<span class="badge badge-danger">Status: Bad</span>';
        }

        return response()->json($response);
    }
}
