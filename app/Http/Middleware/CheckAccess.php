<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Menu;
use App\Models\UserGroup;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $currentPage = $request->path();
        $menu = Menu::where('menu_url',$currentPage)->first();

        if (!$menu) {
            abort(404,'Route Halaman Tidak Ditemukan');
        }

        $menus = UserGroup::find(auth()->user()->user_group_id)->menus;

        $check = $menus->contains('menu_id','=',$menu->menu_id);

        if (!$check) {
            abort(403, 'Anda Tidak Mempunyai Akses ke Halaman Ini.');
        }

        return $next($request);
    }
}